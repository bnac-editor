/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Attribute;
import cl.uchile.dcc.bnac.Capture;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class CaptureView extends ProjectView
    implements ActionListener
{
    protected ArrayList<CaptureBox> captureList;

    protected Box box_list;

    public CaptureView (Project p)
    {
        super(p);
        captureList = new ArrayList<CaptureBox>();
        box_list = new Box(BoxLayout.PAGE_AXIS);

        CaptureBox cb;
        for (Capture c: project.getMuseumEnvironment().captureArray()) {
            cb = new CaptureBox(c);
            captureList.add(cb);
            box_list.add(cb);
        }

        JButton cc = new JButton(Util.gs("CREATE_CAPTURE"));
        cc.addActionListener(this);

        setLayout(new BorderLayout(0, 2));

        add(cc, BorderLayout.NORTH);
        add(new JScrollPane(box_list,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                BorderLayout.CENTER);

    }

    public void addCapture (Capture cap)
    {
        CaptureBox cb = new CaptureBox(cap);
        captureList.add(cb);
        box_list.add(cb);
        ((JScrollPane) getComponent(1)).updateUI();
    }

    class CaptureBox extends JPanel
            implements ActionListener
    {
        protected Capture capture;

        protected JButton bn_plc;
        protected JButton bn_edt;
        protected JButton bn_del;

        public CaptureBox (Capture capture)
        {
            this.capture = capture;

            bn_plc = new JButton(Util.gs("PLACE_CAPTURE"));
            bn_edt = new JButton(Util.gs("EDIT"));
            bn_del = new JButton(Util.gs("DELETE"));

            bn_plc.addActionListener(this);
            bn_edt.addActionListener(this);
            bn_del.addActionListener(this);

            String imgpath = String.format("%s%s%s",
                    project.getRootPath(),
                    System.getProperty("file.separator"),
                    capture.get(Attribute.Preview));
            Image img = null;
            try {
                img = loadImage(new File(imgpath), 100);
            } catch (IOException ioe) {
                BnacEditor.error("could not load preview %s: %s",
                        imgpath, ioe.getMessage());
                img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
            }

            GridBagConstraints c = new GridBagConstraints();
            setLayout(new GridBagLayout());

            c.gridx = c.gridy = 0;
            c.weighty = 1.0;
            c.insets = new Insets(2, 5, 2, 5);
            c.fill = GridBagConstraints.BOTH;
            c.gridwidth = 2;
            c.gridheight = 3;
            add(new JLabel(new ImageIcon(img)), c);

            c.gridx = 2;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridy = GridBagConstraints.RELATIVE;
            c.gridwidth = c.gridheight = 1;
            add(new JLabel(String.format("%s:",
                            BnacEditor.gs("AUTHOR")), JLabel.TRAILING), c);
            add(new JLabel(String.format("%s:",
                            BnacEditor.gs("TITLE")), JLabel.TRAILING), c);
            add(new JLabel(String.format("%s:",
                        BnacEditor.gs("DIMENSIONS")), JLabel.TRAILING), c);

            c.gridx = 3;
            c.gridwidth = 2;
            add(new JLabel(capture.getReferencedObject().get(
                            Attribute.Author)), c);
            add(new JLabel(capture.getReferencedObject().get(
                            Attribute.Title)), c);
            add(new JLabel(String.format("%sm x %sm",
                            capture.getReferencedObject().get(
                                Attribute.Width),
                            capture.getReferencedObject().get(
                                Attribute.Height))), c);

            c.gridx = 5;
            c.gridwidth = 1;
            add(bn_plc, c);
            add(bn_edt, c);
            add(bn_del, c);

            setBorder(BorderFactory.createEtchedBorder());
        }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bn_plc) {
                ProjectDialogue.fireDialogue(
                        new PlaceCaptureDialogue(project, capture,
                            project.getCurrentHallGraph()));
            } else if (e.getSource() == bn_edt) {
            } else if (e.getSource() == bn_del) {
            }
        }
    }

    public void actionPerformed (ActionEvent e)
    {
        ProjectDialogue.fireDialogue(new CreateCaptureDialogue(project));
    }

    protected Image loadImage (File file, int maxlen) throws IOException
    {
        BufferedImage buf = ImageIO.read(file);
        Image img = buf;

        if (buf.getWidth() > maxlen || buf.getHeight() > maxlen) {
            if (buf.getWidth() > buf.getHeight()) {
                img = buf.getScaledInstance(maxlen, -1,
                        BufferedImage.SCALE_FAST);
            } else {
                img = buf.getScaledInstance(-1, maxlen,
                        BufferedImage.SCALE_FAST);
            }
        }

        return img;
    }
}

