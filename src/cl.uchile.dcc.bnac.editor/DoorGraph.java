/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.Door;

import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingBox;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Geometry;
import javax.media.j3d.Material;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;

import javax.vecmath.Matrix3f;
import javax.vecmath.Point3d;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;

public class DoorGraph extends Element2DGraph
{
    protected Door door;
    protected boolean selected;

    protected BranchGroup sel;
    protected BranchGroup norm;

    public DoorGraph (Project project, Door door)
    {
        super(project);
        this.door = door;
        selected = false;

        dims.set(door.getDimensions());
        depth = 0.10f;
        pos.set(door.getPosition());
        angle = door.getAngle();
        scale = door.getScale();

        sel = new BranchGroup();
        sel.setCapability(BranchGroup.ALLOW_DETACH);
        norm = new BranchGroup();
        norm.setCapability(BranchGroup.ALLOW_DETACH);

        rtg.addChild(ftg);
        handle.addChild(rtg);

        buildGraph();
        updatePosition();
        updateDimensions();
        updateAngle();
        updateScale();
    }

    public void buildGraph ()
    {
        Geometry geom = Util.rectMesh(
                dims.x, dims.y,
                -dims.x/2.0f, -dims.y/2.0f, 0.0f, divs);
        Appearance ap;
        Shape3D shape;
       
        ap = new Appearance();
        ap.setMaterial(new Material(
                    new Color3f(1.0f, 1.0f, 1.0f), // ambient
                    new Color3f(0.0f, 0.0f, 0.0f), // emissive
                    new Color3f(1.0f, 1.0f, 1.0f), // diffuse
                    new Color3f(0.0f, 0.0f, 0.0f), // specular
                    0.0f));
        shape = new Shape3D(geom, ap);
        shape.setBounds(new BoundingBox(
                    new Point3d(0.0, 0.0, 0.0),
                    new Point3d(1.2f, 2.0f, -depth)));
        shape.setName(door.getId());
        norm.addChild(shape);

        ap = new Appearance();
        ap.setMaterial(new Material(
                    new Color3f(1.0f, 0.0f, 0.0f), // ambient
                    new Color3f(0.0f, 0.0f, 0.0f), // emissive
                    new Color3f(1.0f, 0.0f, 0.0f), // diffuse
                    new Color3f(0.0f, 0.0f, 0.0f), // specular
                    0.0f));
        shape = new Shape3D(geom, ap);
        shape.setBounds(new BoundingBox(
                    new Point3d(0.0, 0.0, 0.0),
                    new Point3d(1.2f, 2.0f, -depth)));
        shape.setName(door.getId());
        sel.addChild(shape);

        Transform3D t3d = new Transform3D();
        t3d.setTranslation(new Vector3f(door.getPosition3f()));
        rtg.setTransform(t3d);

        frbg = makeFrameGraph();
        rtg.addChild(frbg);
        ftg.addChild(norm);
    }

    public String getId () { return door.getId(); }
    public Door getDoor () { return door; }

    public void select ()
    {
        if (selected) { return; }
        BnacEditor.trace("selected %s", getId());
        selected = true;
        norm.detach();
        ftg.addChild(sel);
    }

    public void deselect ()
    {
        if (!selected) { return; }
        BnacEditor.trace("deselected %s", getId());
        selected = false;
        sel.detach();
        ftg.addChild(norm);
    }

    public void updatePosition ()
    {
        super.updatePosition();
        door.setPosition(pos.x, pos.y);
    }

    public void updateAngle ()
    {
        super.updateAngle();
        door.setAngle(angle);
    }

    public void updateScale ()
    {
        super.updateScale();
        door.setScale(scale);
    }
}

