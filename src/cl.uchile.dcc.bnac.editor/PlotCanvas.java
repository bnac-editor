/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.editor.event.WallEvent;
import cl.uchile.dcc.bnac.editor.event.WallListener;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;

public class PlotCanvas extends Canvas
    implements MouseListener, MouseMotionListener
{
    protected Hall hall;
    protected ArrayList<WallListener> wls;
    int ovrW;

    protected Point2i max;
    protected Point2i border;
    protected Color fgColor;
    protected Color bgColor;

    protected int[] xs;
    protected int[] ys;
    protected int ppm;
    protected Point2i dim;

    public PlotCanvas (Hall hall)
    {
        this(hall, 90, 90, 5, 5);
    }

    public PlotCanvas (Hall hall, int maxX, int maxY,
            int borderX, int borderY)
    {
        this.hall = hall;
        wls = new ArrayList<WallListener>();
        ovrW = -1;

        max = new Point2i(maxX, maxY);
        border = new Point2i(borderX, borderY);
        fgColor = Color.BLACK;
        bgColor = Color.WHITE;

        setSize(maxX+2*borderX, maxY+2*borderY);
        setMaximumSize(getSize());

        updateValues();

        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void updateValues (Hall hall)
    {
        this.hall = hall;
        updateValues();
    }

    public void updateValues ()
    {
        if (hall == null) { return; }

        Point2f[] cs = hall.cornerArray();
        xs = new int[cs.length];
        ys = new int[cs.length];
        Point2f dif = hall.dif();
        Point2f min = hall.min();

        ppm = (int) ((dif.x>dif.y) ? max.x/dif.x : max.y/dif.y);
        dim = new Point2i((int) (dif.x*ppm), (int) (dif.y*ppm));

        Point2i p;
        for (int i=0; i<hall.cornerCount(); ++i) {
            p = coordsToPix(cs[i].x, cs[i].y);
            xs[i] = p.x;
            ys[i] = p.y;
        }
    }

    public Point2i coordsToPix (Point2f cs)
    {
        Point2f min = hall.min();
        return new Point2i(
                border.x + (int) ((cs.x-min.x)*ppm),
                getHeight()-border.y - (int) ((cs.y-min.y)*ppm));
    }

    public Point2f pixToCoords (Point2i ps)
    {
        Point2f min = hall.min();
        return new Point2f(
                ((ps.x-border.x)*1.0f)/ppm + min.x,
                ((getHeight()-border.y-ps.y)*1.0f)/ppm + min.y);
    }

    public Point2i coordsToPix (float x, float y)
    {
        return coordsToPix(new Point2f(x, y));
    }

    public Point2f pixToCoords (int x, int y)
    {
        return pixToCoords(new Point2i(x, y));
    }

    public void paint (Graphics g)
    {
        g.setColor(bgColor);
        g.fillRect(0, 0, max.x+2*border.x, max.y+2*border.y);

        if (hall == null) { return; }

        g.setColor(fgColor);
        g.drawPolygon(xs, ys, xs.length);
    }

    public Image makeImage ()
    {
        BufferedImage img = new BufferedImage(
                max.x+2*border.x, max.y+2*border.y,
                BufferedImage.TYPE_INT_RGB);
        paint(img.getGraphics());
        return img;
    }

    public static Image makeImage (Hall hall, int maxX, int maxY,
            int borderX, int borderY)
    {
        PlotCanvas pc = new PlotCanvas(hall, maxX, maxY, borderX, borderY);
        return pc.makeImage();
    }

    public static Image makeImage (Hall hall)
    {
        PlotCanvas pc = new PlotCanvas(hall);
        return pc.makeImage();
    }

    public void mouseClicked (MouseEvent e)
    {
        if (ovrW > -1) { wallSelected(ovrW, e.getX(), e.getY()); }
    }

    public void mouseEntered (MouseEvent e) { }
    public void mouseExited (MouseEvent e) { }
    public void mousePressed (MouseEvent e) { }
    public void mouseReleased (MouseEvent e) { }

    public void mouseMoved (MouseEvent e)
    {
        if (hall == null) { return; }

        Point2f pos = pixToCoords(e.getX(), e.getY());
        Point2f[] cs = hall.cornerArray();

        if (ovrW > -1 && !Util.isNearLine(cs[ovrW], cs[(ovrW+1)%cs.length],
                    pos, 0.1f)) {
            wallOut(ovrW, e.getX(), e.getY());
            ovrW = -1;
        }

        if (ovrW < 0) {
            for (int i=0; i<cs.length; ++i) {
                if (Util.isNearLine(cs[i], cs[(i+1)%cs.length], pos, 0.1f)) {
                    ovrW = i;
                    wallOver(ovrW, e.getX(), e.getY());
                    break;
                }
            }
        }
    }

    public void mouseDragged (MouseEvent e) { }

    protected void wallSelected (int idx, int x, int y)
    {
        BnacEditor.trace("wallSelected");
        Point2i pix = new Point2i(x, y);
        WallEvent we =
            new WallEvent(hall.getWall(idx), idx, pixToCoords(pix), pix);
        for (WallListener wl: wls) { wl.onWallSelected(we); }
    }

    protected void wallOver (int idx, int x, int y)
    {
        BnacEditor.trace("wallOver");
        Point2i pix = new Point2i(x, y);
        WallEvent we =
            new WallEvent(hall.getWall(idx), idx, pixToCoords(pix), pix);
        for (WallListener wl: wls) { wl.onWallOver(we); }
    }

    protected void wallOut (int idx, int x, int y)
    {
        BnacEditor.trace("wallOut");
        Point2i pix = new Point2i(x, y);
        WallEvent we =
            new WallEvent(hall.getWall(idx), idx, pixToCoords(pix), pix);
        for (WallListener wl: wls) { wl.onWallOut(we); }
    }

    public void addWallListener (WallListener wl) { wls.add(wl); }
    public void removeWallListener (WallListener wl) { wls.remove(wl); }
}

