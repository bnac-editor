/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Attribute;
import cl.uchile.dcc.bnac.Capture;
import cl.uchile.dcc.bnac.PlacedCapture;
import cl.uchile.dcc.bnac.editor.event.WallListener;
import cl.uchile.dcc.bnac.editor.event.WallEvent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.Insets;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javax.vecmath.Point2f;
import javax.vecmath.Vector2f;

public class PlaceCaptureDialogue extends ProjectDialogue
    implements WallListener, MouseMotionListener
{
    protected Capture cap;
    protected HallGraph hg;
    protected WallPickerPlot wpp;
    protected JLabel crds;

    public PlaceCaptureDialogue (Project project, Capture cap, HallGraph hg)
    {
        super(project, BnacEditor.gs("PLACE_CAPTURE"));
        this.cap = cap;
        this.hg = hg;
        wpp = new WallPickerPlot(hg.getHall(), 220, 220, 20, 20);
        crds = new JLabel("x,x");
        crds.setPreferredSize(new java.awt.Dimension(100, 25));

        wpp.addWallListener(this);
        wpp.addMouseMotionListener(this);

        final JButton cancel = new JButton(Util.gs("CANCEL"));
        cancel.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                jf.setVisible(false);
            }
        });

        GridBagLayout lo = new GridBagLayout();
        JPanel pan = new JPanel(lo);
        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(2, 5, 2, 5);
        c.gridx = c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 3;
        pan.add(new JLabel(String.format("%s, %s",
                        cap.getReferencedObject().get(Attribute.Title),
                        cap.getReferencedObject().get(Attribute.Author)),
                    JLabel.CENTER), c);

        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridy = 1;
        c.gridheight = 3;
        pan.add(wpp, c);

        c.weightx = 0.0;
        c.weighty = 0.0;
        c.gridheight = 1;
        c.gridy = 4;
        c.gridx = 0;
        c.gridwidth = 2;
        pan.add(crds, c);

        c.gridx = GridBagConstraints.RELATIVE;
        c.gridwidth = 1;
        pan.add(cancel, c);

        add(pan);
    }

    public void mouseMoved (MouseEvent e)
    {
        Point2f p = wpp.pixToCoords(e.getX(), e.getY());
        crds.setText(String.format("%.02f, %.02f", p.x, p.y));
    }

    public void mouseDragged (MouseEvent e) { }

    public void onWallSelected (WallEvent e)
    {
        float d = e.getWall().origin2f().distance(e.getCoords());
        project.pushCommand(new PlaceCaptureCommand(
                    project, hg, e.getIndex(),
                    String.format("%s_%09d", cap.getId(),
                        System.currentTimeMillis()),
                    cap, new Point2f(d, 1.70f)));
        jf.setVisible(false);
    }

    public void onWallOver (WallEvent e) { }
    public void onWallOut (WallEvent e) { }

    public class PlaceCaptureCommand extends ProjectCommand
    {
        protected HallGraph hg;
        protected int widx;
        protected PortraitGraph pg;

        public PlaceCaptureCommand (Project project, HallGraph hg,
                int widx, String pcapid, Capture cap)
        {
            this(project, hg, widx, pcapid, cap, new Point2f(0.0f, 1.50f));
        }

        public PlaceCaptureCommand (Project project, HallGraph hg,
                int widx, String pcapid, Capture cap, Point2f pos)
        {
            super(project, "PLACE_CAPTURE");
            this.hg = hg;
            this.widx = widx;
            PlacedCapture pcap =
                new PlacedCapture(pcapid, cap, pos, 0.0f, 1.0f);
            pg = new PortraitGraph(project, pcap);
        }

        public void execute ()
        {
            hg.addPortraitGraph(widx, pg);
        }

        public void undo ()
        {
            hg.getWallGraph(widx).removePortraitGraph(pg.getId());
        }
    }
}

