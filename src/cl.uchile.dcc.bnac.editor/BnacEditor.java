/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import java.io.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;

import cl.uchile.dcc.bnac.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class BnacEditor implements Runnable
{
    final static public Properties config = new Properties();
    final static protected Logger l = Logger.getLogger(
            "cl.uchile.dcc.bnac.editor");

    /** Starts the BnacEditor instance.
     */
    static public void start ()
    {
        if (instance != null) {
            BnacEditor.warn("BnacEditor.start called for a second time");
            return;
        }

        BnacEditor.info("starting BnacEditor r%s",
                "$Rev: 83 $".replaceAll("\\$Rev: (\\d+) \\$", "$1"));
        instance = new BnacEditor();
        SwingUtilities.invokeLater(instance);
    }
    
    /** Returns the required string from the resource bundle.
     * @param key   the key of the desired string
     * @return the string from the resource bundle, or an empty string if that
     * fails
     */
    static public String gs (String key)
    {
        try {
            return bundle.getString(key);
        } catch (Exception e) {
            BnacEditor.error("could not load string %s: %s",
                    key, e.getMessage());
            return key;
        }
    }

    /** Returns the undo action.
     * @return the undo action
     */
    static public Action getUndoAction () { return instance.undo; }

    /** Returns the redo action.
     * @return the redo action
     */
    static public Action getRedoAction () { return instance.redo; }

    /** The resource bundle. */
    static public ResourceBundle bundle = ResourceBundle.getBundle(
            "cl.uchile.dcc.bnac.editor.data.txt.common");

    /** The singleton instance. */
    static protected BnacEditor instance;

    /** The frame with the tools. */
    protected JFrame toolFrame;

    /** The panel with the tools. */
    protected ToolPanel tools;

    /** Open projects. */
    protected ArrayList<Project> projects;

    /** Undo action */
    protected Action undo;

    /** Redo action */
    protected Action redo;

    /** Protected constructor.
     */
    protected BnacEditor ()
    {
        projects = new ArrayList<Project>();
        buildMainToolPanel();
    }

    /** Creates the main tool panel.
     */
    protected void buildMainToolPanel ()
    {
        toolFrame = new JFrame(gs("APP_TITLE"));

        toolFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        toolFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e)
            {
                BnacEditor.quit();
            }
        });

        String[] menus = { "FILE", "EDIT", "VIEW", "TOOLS", "HELP" };
        ProjectAction[][] menuItems = {
            {
                ProjectAction.New,
                ProjectAction.Open,
                null,
                ProjectAction.Save,
                null,
                ProjectAction.Quit
            },
            {
                ProjectAction.Undo,
                ProjectAction.Redo
            },
            {
                ProjectAction.SimulationView,
                ProjectAction.CaptureView,
                ProjectAction.HallView,
                ProjectAction.PlotView,
                ProjectAction.NavigationView,
                ProjectAction.RouteView
            },
            {
                ProjectAction.Translate,
                ProjectAction.Info
            },
            {
                ProjectAction.About
            }
        };

        toolFrame.setJMenuBar(new JMenuBar());
        for (int i=0; i<menus.length; ++i) {
            JMenu menu = new JMenu(Util.gs(menus[i]));
            menu.setMnemonic(KeyStroke.getKeyStroke(
                        Util.gs(menus[i] + "_MN")).getKeyCode());

            for (ProjectAction pat: menuItems[i]) {
                if (pat != null) {
                    Action ac = pat.getAction();
                    menu.add(ac);
                    switch (pat) {
                        case Undo:
                            undo = ac;
                            break;
                        case Redo:
                            redo = ac;
                            break;
                        default:
                            break;
                    }
                } else {
                    menu.add(new JSeparator());
                }
            }

            toolFrame.getJMenuBar().add(menu);
        }

        toolFrame.add((tools = new ToolPanel()));

        toolFrame.pack();
        toolFrame.setMinimumSize(toolFrame.getSize());
    }

    /** Run method override.
     */
    public void run ()
    {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            BnacEditor.warn(e.getMessage());
        }
        toolFrame.setVisible(true);
    }

    static public void saveProject ()
    {
        try {
            BnacEditor.getSelectedProject().save();
        } catch (IOException ioe) {
            BnacEditor.error("%s", ioe.getMessage());
        }
    }
    
    static public void openProject ()
    {
        JFileChooser jfc =
            new JFileChooser(BnacEditor.config.getProperty("user.dir"));
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int rc = jfc.showOpenDialog(null);
        if (rc == JFileChooser.APPROVE_OPTION) {
            try {
                BnacEditor.addProject(Project.fromPath(
                            jfc.getSelectedFile().getAbsolutePath()));
            } catch (Exception ex) {
                BnacEditor.error(ex.getMessage());
            }
        }
    }

    static public boolean closeProject ()
    {
        Project pr = BnacEditor.getSelectedProject();
        pr.close();
        instance.projects.remove(pr);
        instance.tools.pcb.removeItem(pr);

        return true;
    }

    static public Project getSelectedProject ()
    {
        return (Project) instance.tools.pcb.getSelectedItem();
    }

    static public void selectProject (Project p)
    {
        instance.tools.pcb.setSelectedItem(p);
        p.activate();
    }

    static public boolean autoFocus ()
    {
        return instance.tools.ab.isSelected();
    }

    static public void addProject (Project p)
    {
        BnacEditor.info("adding project %s", p.toString());
        if (p.getRootPath() != null) {
            for (Project  pr: instance.projects) {
                if (p.getRootPath().equals(pr.getRootPath())) {
                    selectProject(pr);
                    return;
                }
            }
        }

        instance.projects.add(p);
        instance.tools.pcb.addItem(p);
        instance.tools.pcb.setSelectedItem(p);
    }

    static public ArrayList<Project> getProjects ()
    {
        return instance.projects;
    }

    static public void quit ()
    {
        BnacEditor.info("ending BnacEditor");
        System.exit(0);
    }

    static public void main (String[] args) throws Exception
    {
        BnacEditor.config.put("user.dir", System.getProperty("user.dir"));
        BnacEditor.config.put("user.home", System.getProperty("user.home"));
        BnacEditor.config.put("user.name", System.getProperty("user.name"));
        BnacEditor.config.put("last.dir", System.getProperty("user.home"));

        if (System.getProperty("bnac.log4j") != null) {
            PropertyConfigurator.configure(System.getProperty("bnac.log4j"));
        }

        BnacEditor.start();

        for (String file: args) {
            BnacEditor.addProject(Project.fromPath(file));
        }
    }

    static public void trace (String s, Throwable t) { l.trace(s, t); }
    static public void trace (String s, Throwable t, Object... args)
    {
        l.trace(String.format(s, args), t);
    }
    static public void trace (String s) { l.trace(s); }
    static public void trace (String s, Object... args)
    {
        l.trace(String.format(s, args));
    }
    static public void debug (String s) { l.debug(s); }
    static public void debug (String s, Object... args)
    {
        l.debug(String.format(s, args));
    }
    static public void info (String s) { l.info(s); }
    static public void info (String s, Object... args)
    {
        l.info(String.format(s, args));
    }
    static public void warn (String s) { l.warn(s); }
    static public void warn (String s, Object... args)
    {
        l.warn(String.format(s, args));
    }
    static public void error (String s, Throwable t) { l.error(s, t); }
    static public void error (String s, Throwable t, Object... args)
    {
        l.error(String.format(s, args), t);
    }
    static public void error (String s) { l.error(s); }
    static public void error (String s, Object... args)
    {
        l.error(String.format(s, args));
    }
    static public void fatal (String s) { l.fatal(s); }
    static public void fatal (String s, Object... args)
    {
        l.fatal(String.format(s, args));
    }
}

class ToolPanel extends Box
{
    JTabbedPane tabs;
    JComboBox pcb;
    JToggleButton ab;

    public ToolPanel ()
    {
        super(BoxLayout.PAGE_AXIS);

        Box headBox = new Box(BoxLayout.LINE_AXIS);

        pcb = new JComboBox();
        ab = new JToggleButton(Util.gs("AUTO"));
        ab.setSelected(true);
        ab.setToolTipText(Util.gs("AUTO_SD"));

        headBox.add(pcb);
        headBox.add(Box.createHorizontalStrut(5));
        headBox.add(ab);

        this.add(headBox);
        this.add(Box.createVerticalGlue());
        this.add((tabs = new JTabbedPane()));

        headBox.setMaximumSize(new Dimension(
                    0x4fffffff,
                    headBox.getPreferredSize().height));

        this.setPreferredSize(new Dimension(
                    headBox.getPreferredSize().width*2,
                    headBox.getPreferredSize().height*10));
    }

    public void addTool (Tool t)
    {
        /* TODO placeholder icon for iconless tools */

        Box tab = new Box(BoxLayout.PAGE_AXIS);
        Box hdr = new Box(BoxLayout.LINE_AXIS);

        hdr.add(new JLabel(t.getTitle()));
        hdr.add(Box.createHorizontalGlue());

        tab.add(hdr);
        tab.add(t);

        tabs.addTab(t.getTitle(), t.getIcon(), tab, t.getTip());
    }

    public void add (Tool t) { addTool(t); }
}

abstract class Tool extends JPanel
{
    protected String title;
    protected Icon icon;
    protected String tip;

    public Tool (String title, Icon icon, String tip)
    {
        this.title = title;
        this.icon = icon;
        this.tip = tip;

        buildUi();
    }

    public String getTitle () { return title; }
    public Icon getIcon () { return icon; }
    public String getTip () { return tip; }

    abstract void buildUi ();
}

enum ProjectAction
{
    /* File */
    New ("NEW"),
    Open ("OPEN"),
    Save ("SAVE"),
    SaveAs ("SAVE_AS"),
    Close ("CLOSE"),
    CloseAll ("CLOSE_ALL"),
    Quit ("QUIT"),
    /* Edit */
    Undo ("UNDO"),
    Redo ("REDO"),
    /* View */
    SimulationView ("SIMULATIONV"),
    PlotView ("PLOTV"),
    CaptureView ("CAPTUREV"),
    HallView ("HALLV"),
    NavigationView ("NAVIGATIONV"),
    RouteView ("ROUTEV"),
    /* Tools */
    Translate ("TRANSLATE"),
    Info ("INFO"),
    /* Help */
    About ("ABOUT");

    protected String propKey;

    ProjectAction (String propKey) { this.propKey = propKey; }

    public String getPropertyKey () { return propKey; }

    public Action getAction ()
    {
        Action ac;
        switch (this) {
            case Open:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        BnacEditor.openProject();
                    }
                };
                break;
            case Save:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.saveProject();
                    }
                };
                break;
            case Quit:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        BnacEditor.quit();
                    }
                };
                break;
            case SimulationView:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.getSelectedProject().
                            setSimulationViewVisible(true);
                    }
                };
                break;
            case PlotView:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.
                            getSelectedProject().setPlotViewVisible(true);
                    }
                };
                break;
            case CaptureView:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.
                            getSelectedProject().setCaptureViewVisible(true);
                    }
                };
                break;
            case HallView:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.
                            getSelectedProject().setHallViewVisible(true);
                    }
                };
                break;
            case NavigationView:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.getSelectedProject().
                            setNavigationViewVisible(true);
                    }
                };
                break;
            case RouteView:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.getSelectedProject().
                            setRouteViewVisible(true);
                    }
                };
                break;
            case Translate:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() != null) {
                            Util.showFrame(BnacEditor.gs("TRANSLATE"),
                                    new TranslateTool(
                                        BnacEditor.getSelectedProject()));
                        }
                    }
                };
                break;
            case Info:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() != null) {
                            Util.showFrame(BnacEditor.gs("INFO"),
                                    new InfoTool(
                                        BnacEditor.getSelectedProject()));
                        }
                    }
                };
                break;
            case New:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        ProjectDialogue.fireDialogue(
                                new CreateProjectDialogue());
                    }
                };
                break;
            //case DataView:
            case SaveAs:
            case Close:
            case CloseAll:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        BnacEditor.warn("TODO: %s", name());
                    }
                };
                break;
            case Undo:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.getSelectedProject().undo();
                    }
                };
                break;
            case Redo:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        if (BnacEditor.getSelectedProject() == null) {
                            return;
                        }
                        BnacEditor.getSelectedProject().redo();
                    }
                };
                break;
            case About:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        JFrame jf = new JFrame(Util.gs("ABOUT"));
                        Box b = new Box(BoxLayout.LINE_AXIS);
                        b.setBorder(BorderFactory.createEmptyBorder(
                                    15, 15, 15, 15));
                        String txt = String.format(
                                "<html><div " +
                                "style=margin-right:auto;margin-left:auto;" +
                                "padding:2em;text-align:center;" +
                                "<h1 style=text-align:center>" +
                                "<b>%s</b></h1>" +
                                "%s<br>" +
                                "<div " +
                                "style=font-size:0.9em;text-align:center>" +
                                "%s</div>" +
                                "</div>" +
                                "<div " +
                                "style=font-size:0.9em;text-align:center>" +
                                "v%s (r%s)</div>" +
                                "</html>",
                                Util.gs("APP_TITLE"),
                                Util.gs("APP_DESC"),
                                Util.gs("APP_C"),
                                Util.gs("APP_V"),
                                "$Rev: 83 $".replaceAll(
                                    "\\$Rev: (\\d+) \\$", "$1"));
                        b.add(new JLabel(txt));
                        jf.add(b);
                        jf.pack();
                        jf.setVisible(true);
                    }
                };
                break;
            default:
                ac = new AbstractAction () {
                    public void actionPerformed (ActionEvent e) {
                        BnacEditor.warn("TODO: %s", name());
                    }
                };
                break;
        }

        ac.putValue(Action.NAME, Util.gs(propKey));
        ac.putValue(Action.SHORT_DESCRIPTION, Util.gs(propKey + "_SD"));
        try {
            BnacEditor.debug("Accel for %s: %s",
                    propKey, BnacEditor.gs(propKey + "_ACCEL"));
            ac.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                        Util.gs(propKey + "_ACCEL")));
        } catch (Exception e) { BnacEditor.warn(e.getMessage()); }
        try {
            ac.putValue(Action.MNEMONIC_KEY, KeyStroke.getKeyStroke(
                        Util.gs(propKey + "_MN")).getKeyCode());
        } catch (Exception e) {
            BnacEditor.warn(e.getMessage());
        }

        return ac;
    }
}

