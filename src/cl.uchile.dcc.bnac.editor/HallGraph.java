/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.Wall;
import cl.uchile.dcc.bnac.Door;
import cl.uchile.dcc.bnac.PlacedCapture;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.vecmath.Point2f;
import javax.vecmath.Vector2f;

import javax.media.j3d.BranchGroup;

public class HallGraph extends SceneElementGraph
{
    protected Hall hall;

    protected ArrayList<WallGraph> wgs;
    protected BranchGroup floor;
    protected BranchGroup ceil;

    public HallGraph (Project project, String id)
    {
        this(project, new Hall(id));
    }

    public HallGraph (Project project, Hall hall)
    {
        super(project);
        this.hall = hall;

        wgs = new ArrayList<WallGraph>();

        int i = 0;
        for (Wall wall: hall.wallArray()) {
            wgs.add(new WallGraph(project, wall, hall.getHeight(),
                        String.format("wall-%02d", i++)));
        }

        buildGraph();
    }

    protected void buildGraph ()
    {
        for (WallGraph wg: wgs) { handle.addChild(wg.getHandle()); }
        floor = Util.makeFloor(hall.cornerArray(), 50, 50);
        floor.setCapability(BranchGroup.ALLOW_DETACH);
        ceil = Util.makeCeiling(hall.cornerArray(), hall.getHeight(), 50, 50);
        ceil.setCapability(BranchGroup.ALLOW_DETACH);
        handle.addChild(floor);
        handle.addChild(ceil);
    }

    public void setHeight (float height)
    {
        hall.setHeight(height);
        for (WallGraph wg: wgs) { wg.setHeight(height); }
        update();
    }

    public void update ()
    {
        handle.removeAllChildren();
        buildGraph();
    }

    protected void updateFloorAndCeiling ()
    {
        floor.detach();
        ceil.detach();
        floor = Util.makeFloor(hall.cornerArray(), 50, 50);
        floor.setCapability(BranchGroup.ALLOW_DETACH);
        ceil = Util.makeCeiling(hall.cornerArray(), hall.getHeight(), 50, 50);
        ceil.setCapability(BranchGroup.ALLOW_DETACH);
        handle.addChild(floor);
        handle.addChild(ceil);
    }

    public void moveCorner (int idx, Point2f dst)
    {
        hall.moveCorner(idx, dst);
        wgs.get((idx>0)?idx-1:wgs.size()-1).update();
        wgs.get(idx).update();
        updateFloorAndCeiling();
    }

    public void addCorner (int idx, Point2f p)
    {
        WallGraph wg = wgs.get((idx>0)?idx-1:wgs.size()-1);
        WallGraph newwg = new WallGraph(project, hall.addCorner(idx, p),
                hall.getHeight(), String.format("wall-%02d", idx));
        wgs.add(idx, newwg);
        handle.addChild(newwg.getHandle());
        wg.update();
        for (int i=idx+1; i<wgs.size(); ++i) {
            wgs.get(i).setId(String.format("wall-%02d", i));
        }
        updateFloorAndCeiling();
    }

    public WallGraph[] removeCorner (int idx)
    {
        if (wgs.size() > 3) {
            WallGraph[] rw = new WallGraph[2];
            WallGraph newwg = new WallGraph(project,
                    hall.removeCorner(idx), hall.getHeight(),
                    String.format("wall-%02d", (idx>0)?idx-1:wgs.size()-1));
            if (idx > 0) {
                rw[0] = wgs.remove(idx-1);
                rw[1] = wgs.remove(idx-1);
                wgs.add(idx-1, newwg);
            } else {
                rw[0] = wgs.get(wgs.size()-1);
                rw[1] = wgs.get(0);
                wgs.remove(0);
                wgs.remove(wgs.size()-1);
                wgs.add(wgs.size(), newwg);
            }
            BnacEditor.debug("saving" +
                    " (%.02f,%.02f)-(%.02f,%.02f)" +
                    " (%.02f,%.02f)-(%.02f,%.02f)",
                    rw[0].getWall().origin2f().x,
                    rw[0].getWall().origin2f().y,
                    rw[0].getWall().end2f().x, rw[0].getWall().end2f().y,
                    rw[1].getWall().origin2f().x,
                    rw[1].getWall().origin2f().y,
                    rw[1].getWall().end2f().x, rw[1].getWall().end2f().y);

            rw[0].detach();
            rw[1].detach();
            handle.addChild(newwg.getHandle());
            updateFloorAndCeiling();

            return rw;
        }
        return null;
    }

    public WallGraph setWall (int idx, WallGraph wg)
    {
        hall.setWall(idx, wg.getWall());
        WallGraph ret = wgs.set(idx, wg);
        ret.detach();
        handle.addChild(wg.getHandle());
        return ret;
    }

    public PortraitGraph getPortraitGraph (String id)
    {
        for (WallGraph wg: wgs) {
            for (PortraitGraph pg: wg.portraitGraphArray()) {
                if (pg.getId().equals(id)) { return pg; }
            }
        }
        return null;
    }

    public DoorGraph getDoorGraph (String id)
    {
        for (WallGraph wg: wgs) {
            for (DoorGraph dg: wg.doorGraphArray()) {
                if (dg.getId().equals(id)) { return dg; }
            }
        }
        return null;
    }

    public WallGraph getWallGraph (int idx) { return wgs.get(idx); }

    public WallGraph[] wallGraphArray ()
    {
        return wgs.toArray(new WallGraph[0]);
    }

    public Hall getHall () { return hall; }

    public Point2f[] cornerArray () { return hall.cornerArray(); }

    public int size () { return wgs.size(); }

    public String getId () { return hall.getId(); }

    public void addPortraitGraph (int widx, PortraitGraph pg)
    {
        wgs.get(widx).addPortraitGraph(pg);
    }

    public PortraitGraph removePortraitGraph (String id)
    {
        PortraitGraph ret = getPortraitGraph(id);
        if (ret != null) {
            wgs.get(hall.getParentWall(id)).removePortraitGraph(id);
        }
        return ret;
    }

    public void addDoorGraph (int widx, DoorGraph pg)
    {
        wgs.get(widx).addDoorGraph(pg);
    }

    public DoorGraph removeDoorGraph (String id)
    {
        DoorGraph ret = getDoorGraph(id);
        if (hall.getDoor(id) != null) {
            wgs.get(hall.getParentWall(id)).removeDoor(id);
        }
        return ret;
    }

    public Element2DGraph getElem2DGraph (String id)
    {
        for (WallGraph wg: wgs) {
            for (PortraitGraph pg: wg.portraitGraphArray()) {
                if (pg.getId().equals(id)) { return pg; }
            }
            for (DoorGraph dg: wg.doorGraphArray()) {
                if (dg.getId().equals(id)) { return dg; }
            }
        }
        return null;
    }
}

