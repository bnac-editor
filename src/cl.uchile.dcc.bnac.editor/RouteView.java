/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Capture;
import cl.uchile.dcc.bnac.Capture2D;
import cl.uchile.dcc.bnac.Route;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.Box;
import javax.swing.BoxLayout;

public class RouteView extends ProjectView implements ActionListener
{
    protected ArrayList<RouteBox> routeList;
    protected Box box_list;

    public RouteView (Project project)
    {
        super(project);

        routeList = new ArrayList<RouteBox>();
        box_list = new Box(BoxLayout.PAGE_AXIS);

        buildUi();

        RouteBox rb;
        for (Route r: project.getMuseumEnvironment().routeArray()) {
            rb = new RouteBox(r);
            routeList.add(rb);
            box_list.add(rb);
        }
        ((JScrollPane) getComponent(1)).updateUI();
    }

    protected void buildUi ()
    {
        JButton nr = new JButton(BnacEditor.gs("ADD_ROUTE"));
        nr.addActionListener(this);

        setLayout(new BorderLayout(0, 2));
        add(nr, BorderLayout.NORTH);
        add(new JScrollPane(box_list,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                BorderLayout.CENTER);
    }

    public void addRoute (Route r)
    {
        RouteBox rb = new RouteBox(r);
        routeList.add(rb);
        box_list.add(rb);
        ((JScrollPane) getComponent(1)).updateUI();
    }

    public Route removeRoute (String id)
    {
        RouteBox rb = null;
        for (RouteBox rbb: routeList) {
            if (rbb.getRouteId().equals(id)) {
                rb = rbb;
                routeList.remove(rb);
                break;
            }
        }

        if (rb != null) {
            box_list.remove(rb);
            ((JScrollPane) getComponent(1)).updateUI();
        }

        return null;
    }

    public void actionPerformed (ActionEvent e)
    {
        ProjectDialogue.fireDialogue(new CreateRouteDialogue(project));
    }

    class RouteBox extends JPanel implements ActionListener
    {
        protected Route route;

        protected JButton bn_edit;
        protected JButton bn_del;

        public RouteBox (Route route)
        {
            super(new GridBagLayout());
            this.route = route;

            bn_edit = new JButton(BnacEditor.gs("EDIT"));
            bn_del = new JButton(BnacEditor.gs("DELETE"));

            buildUi();
            
            bn_edit.addActionListener(this);
            bn_del.addActionListener(this);
        }

        protected void buildUi ()
        {
            GridBagConstraints c = new GridBagConstraints();

            c.anchor = GridBagConstraints.LINE_START;
            c.gridx = c.gridy = 0;
            c.gridwidth = 3;
            c.gridheight = 1;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(2, 5, 2, 5);
            c.weightx = 1.0;
            c.weighty = 1.0;
            c.ipady = 3;
            add(new JLabel(route.getId(), JLabel.LEADING), c);

            c.ipady = 0;
            c.anchor = GridBagConstraints.LINE_END;
            c.fill = GridBagConstraints.NONE;
            c.weightx = 0.0;
            c.gridx = GridBagConstraints.RELATIVE;
            c.gridwidth = 1;
            add(bn_edit, c);
            add(bn_del, c);

            setBorder(BorderFactory.createEtchedBorder());

            java.awt.Dimension d = getPreferredSize();
            d.width = Integer.MAX_VALUE;
            setMaximumSize(d);
        }

        public String getRouteId () { return route.getId(); }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bn_edit) {
                ProjectDialogue.fireDialogue(
                        new RouteDialogue(project, route));
            } else if (e.getSource() == bn_del) {
                project.pushCommand(new RemoveRouteCommand(project, route));
            }
        }
    }

    class CreateRouteDialogue extends ProjectDialogue
            implements ActionListener
    {
        protected JTextField tf_ttl;
        protected JButton bn_ok;
        protected JButton bn_ccl;

        public CreateRouteDialogue (Project p)
        {
            super(p, BnacEditor.gs("ADD_ROUTE"));

            tf_ttl = new JTextField();
            bn_ok = new JButton(BnacEditor.gs("OK"));
            bn_ccl = new JButton(BnacEditor.gs("CANCEL"));

            buildUi();

            bn_ok.addActionListener(this);
            bn_ccl.addActionListener(this);
        }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bn_ok) {
                project.pushCommand(new AddRouteCommand(project,
                            new Route(Util.sanitize(tf_ttl.getText()))));
            }
            jf.setVisible(false);
        }

        protected void buildUi ()
        {
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.insets = new Insets(2, 5, 2, 5);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.weighty = 1.0;
            c.gridx = c.gridy = 0;
            c.gridwidth = 1;
            add(new JLabel(BnacEditor.gs("TITLE")), c);

            c.weightx = 1.0;
            c.gridx = GridBagConstraints.RELATIVE;
            c.gridwidth = 3;
            add(tf_ttl, c);

            c.weightx = 0.0;
            c.gridy = 1;
            c.gridx = 2;
            c.weightx = 0.0;
            c.gridwidth = 1;
            add(bn_ccl, c);

            c.gridx = GridBagConstraints.RELATIVE;
            add(bn_ok, c);
        }
    }

    class AddRouteCommand extends ProjectCommand
    {
        protected Route route;

        public AddRouteCommand (Project project, Route route)
        {
            super(project, "ADD_ROUTE");
            this.route = route;
        }

        public void execute ()
        {
            BnacEditor.trace("do AddRouteCommand");
            addRoute(route);
            project.getMuseumEnvironment().addRoute(route);
        }

        public void undo ()
        {
            BnacEditor.trace("undo AddRouteCommand");
            removeRoute(route.getId());
            project.getMuseumEnvironment().removeRoute(route.getId());
        }
    }

    class RemoveRouteCommand extends ProjectCommand
    {
        protected Route route;

        public RemoveRouteCommand (Project project, Route route)
        {
            super(project, "REMOVE_ROUTE");
            this.route = route;
        }

        public void execute ()
        {
            BnacEditor.trace("do RemoveRouteCommand");
            removeRoute(route.getId());
            project.getMuseumEnvironment().removeRoute(route.getId());
        }

        public void undo ()
        {
            BnacEditor.trace("undo RemoveRouteCommand");
            addRoute(route);
            project.getMuseumEnvironment().addRoute(route);
        }
    }
}

