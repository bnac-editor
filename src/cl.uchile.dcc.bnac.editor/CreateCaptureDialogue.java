/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Capture2D;
import cl.uchile.dcc.bnac.CObject;
import cl.uchile.dcc.bnac.Attribute;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

public class CreateCaptureDialogue extends ProjectDialogue
    implements ActionListener
{
    protected JTextField tf_tit;
    protected JTextField tf_aut;
    protected JTextField tf_typ;
    protected JTextField tf_wid;
    protected JTextField tf_hei;
    protected JTextField tf_rsc;
    protected JTextField tf_rnm;
    protected JButton bn_brw;
    protected JButton bn_ccl;
    protected JButton bn_add;

    public CreateCaptureDialogue (Project project)
    {
        super(project, BnacEditor.gs("CREATE_CAPTURE"));

        tf_aut = new JTextField(20);
        tf_tit = new JTextField(20);
        tf_typ = new JTextField(20);
        tf_rsc = new JTextField(15);
        tf_rnm = new JTextField(20);
        tf_wid = new JTextField();
        tf_hei = new JTextField();
        bn_brw = new JButton(BnacEditor.gs("BROWSE"));
        bn_ccl = new JButton(BnacEditor.gs("CANCEL"));
        bn_add = new JButton(BnacEditor.gs("ADD"));

        buildForm();

        bn_brw.addActionListener(this);
        bn_ccl.addActionListener(this);
        bn_add.addActionListener(this);
    }

    protected void buildForm ()
    {
        setLayout(new GridBagLayout());

        JLabel xlbl = new JLabel("x", JLabel.CENTER);
        String[] strs = {
            BnacEditor.gs("AUTHOR"),
            BnacEditor.gs("TITLE"),
            BnacEditor.gs("TYPE"),
            BnacEditor.gs("RESOURCE"),
            BnacEditor.gs("RENAME"),
            BnacEditor.gs("DIMENSIONS")
        };
        JLabel[] lbls = new JLabel[6];
        for (int i=0; i<lbls.length; ++i) {
            lbls[i] = new JLabel(String.format("%s:", strs[i]),
                    JLabel.TRAILING);
        }

        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(2, 5, 2, 5);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.weighty = 1.0;
        c.gridx = c.gridy = 0;
        add(lbls[0], c);

        c.weightx = 1.0;
        c.gridx = GridBagConstraints.RELATIVE;
        c.gridwidth = 3;
        add(tf_aut, c);

        c.weightx = 0.0;
        c.gridwidth = 1;
        add(lbls[3], c);

        c.weightx = 1.0;
        c.gridwidth = 2;
        add(tf_rsc, c);

        c.weightx = 0.0;
        c.gridwidth = 1;
        add(bn_brw, c);

        c.gridy = 1;
        add(lbls[1], c);

        c.weightx = 1.0;
        c.gridwidth = 3;
        add(tf_tit, c);

        c.weightx = 0.0;
        c.gridwidth = 1;
        add(lbls[4], c);

        c.weightx = 1.0;
        c.gridwidth = 3;
        add(tf_rnm, c);

        c.gridy = 2;
        c.weightx = 0.0;
        c.gridwidth = 1;
        add(lbls[2], c);

        c.weightx = 1.0;
        c.gridwidth = 3;
        add(tf_typ, c);

        c.weightx = 0.0;
        c.gridwidth = 1;
        add(lbls[5], c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 1;
        add(tf_wid, c);

        c.weightx = 0.5;
        add(xlbl, c);

        c.weightx = 0.5;
        add(tf_hei, c);

        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 4;
        add(bn_add, c);
        c.gridx = 4;
        add(bn_ccl, c);
    }

    public void actionPerformed (ActionEvent e)
    {
        if (e.getSource() == bn_brw) {
            JFileChooser jfc = new JFileChooser(
                    BnacEditor.config.getProperty("last.dir"));
            jfc.setFileFilter(new FileNameExtensionFilter(
                        Util.gs("IMAGE_FILES"), "jpg", "jpeg", "png"));
            ImagePreview ip = new ImagePreview();
            jfc.addPropertyChangeListener(ip);
            jfc.setAccessory(ip);

            if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                tf_rsc.setText(jfc.getSelectedFile().getAbsolutePath());
                tf_rnm.setText(jfc.getSelectedFile().getName());
                BnacEditor.config.setProperty("last.dir",
                    jfc.getSelectedFile().getParent());
            }
        } else if (e.getSource() == bn_ccl) {
            jf.setVisible(false);
        } else {
            File imgOrig = new File(tf_rsc.getText());
            File imgDest = new File(
                    new File(String.format("%s/img", project.getRootPath())),
                    tf_rnm.getText());

            try {
                if (imgDest.createNewFile()) {
                    try {
                        FileInputStream fin = new FileInputStream(imgOrig);
                        FileOutputStream fout = new FileOutputStream(imgDest);

                        int b;
                        while ((b = fin.read()) > -1) { fout.write(b); }

                        CObject obj =
                            new CObject(Util.sanitize(tf_tit.getText()));
                        obj.set(Attribute.Author, tf_aut.getText());
                        obj.set(Attribute.Title, tf_tit.getText());
                        obj.set(Attribute.Type, tf_typ.getText());
                        obj.set(Attribute.Width, tf_wid.getText());
                        obj.set(Attribute.Height, tf_hei.getText());

                        Capture2D cap = new Capture2D(
                                String.format("cap_%s", obj.getId()), obj);
                        cap.set(Attribute.Type, "img/png");
                        GregorianCalendar cal = new GregorianCalendar();
                        cap.set(Attribute.Date,
                                String.format("%d-%02d-%02d",
                                    cal.get(GregorianCalendar.YEAR),
                                    cal.get(GregorianCalendar.MONTH)+1,
                                    cal.get(GregorianCalendar.DAY_OF_MONTH)));
                        cap.set(Attribute.Resource,
                                String.format("img/%s", tf_rnm.getText()));
                        cap.set(Attribute.Preview,
                                String.format("img/%s", tf_rnm.getText()));

                        project.addObject(obj);
                        project.addCapture(cap);
                    } catch (FileNotFoundException fnfe) {
                        System.out.printf("%s\n", fnfe.getMessage());
                    } catch (IOException ioe) {
                        System.out.printf("%s\n", ioe.getMessage());
                    }
                } else {
                    System.out.printf("%s\n", Util.gs("ERROR"));
                }
            } catch (IOException ioe) {
                System.out.printf("%s\n", ioe.getMessage());
                return;
            }

            jf.setVisible(false);
        }
    }

    class ImagePreview extends JComponent
            implements PropertyChangeListener
    {
        Image img;

        public ImagePreview ()
        {
            setPreferredSize(new Dimension(150, 150));
        }

        public void paintComponent (Graphics g)
        {
            if (img != null) { g.drawImage(img, 10, 10, 130, 130, this); }
        }

        public void propertyChange (PropertyChangeEvent e)
        {
            if (e.getPropertyName().equals(
                        JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)) {
                try {
                    img = ImageIO.read((File) e.getNewValue()).
                        getScaledInstance(130, -1, BufferedImage.SCALE_FAST);
                    repaint();
                } catch (Exception ex) {
                    BnacEditor.error("can't load %s: %s",
                            ((File) e.getNewValue()).getAbsolutePath(),
                            ex.getMessage());
                }
            } else if (e.getPropertyName().equals(
                        JFileChooser.DIRECTORY_CHANGED_PROPERTY)) {
                img = null;
                repaint();
            }
        }
    }
}

