/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Geometry;
import javax.media.j3d.Material;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Point2f;
import javax.vecmath.Vector3f;

abstract class Element2DGraph extends SceneElementGraph
{
    protected Point2f dims;
    protected float depth;
    protected Point2f pos;
    protected float angle;
    protected float scale;
    protected int divs;

    protected TransformGroup rtg;
    protected TransformGroup ftg;
    protected BranchGroup frbg;

    public Element2DGraph (Project project)
    {
        this(project, new Point2f(1.0f, 1.0f), 0.10f,
                new Point2f(0.0f, 0.0f), 0.0f, 1.0f, 50);
    }

    public Element2DGraph (Project project, Point2f dims, float depth,
            Point2f pos, float angle, float scale, int divs)
    {
        super(project);
        this.dims = new Point2f(dims);
        this.depth = depth;
        this.pos = new Point2f(pos);
        this.angle = angle;
        this.scale = scale;
        this.divs = divs;

        rtg = new TransformGroup();
        rtg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        rtg.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        rtg.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);

        ftg = new TransformGroup();
        ftg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        ftg.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        ftg.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
    }

    abstract public void select ();
    abstract public void deselect ();

    public void updateDimensions ()
    {
        Transform3D t3d = new Transform3D();
        ftg.getTransform(t3d);
        t3d.setTranslation(new Vector3f(0.0f, 0.0f, depth));
        ftg.setTransform(t3d);

        if (frbg != null) { frbg.detach(); }
        frbg = makeFrameGraph();
        rtg.addChild(frbg);
    }
    public void editDimensions (Point2f p)
    {
        dims.add(p);
        updateDimensions();
    }
    public void setDimensions (Point2f p)
    {
        dims.set(p);
        updateDimensions();
    }
    public void editDepth (float d)
    {
        depth += d;
        updateDimensions();
    }
    public void setDepth (float d) 
    { 
        depth = d; 
        updateDimensions();
    }

    protected void updatePosition ()
    {
        Transform3D t3d = new Transform3D();
        rtg.getTransform(t3d);
        t3d.setTranslation(new Vector3f(pos.x, pos.y, 0.0f));
        rtg.setTransform(t3d);
    }
    public void editPosition (Point2f v)
    {
        pos.add(v);
        updatePosition();
    }
    public void setPosition (Point2f v)
    {
        pos.set(v);
        updatePosition();
    }

    protected void updateAngle ()
    {
        Transform3D t3d = new Transform3D();
        rtg.getTransform(t3d);
        Matrix3f rot = new Matrix3f();
        rot.rotZ(angle);
        t3d.setRotation(rot);
        rtg.setTransform(t3d);
    }
    public void editAngle (float ang)
    {
        angle += ang;
        updateAngle();
    }
    public void setAngle (float ang) 
    { 
        angle = ang; 
        updateAngle();
    }

    protected void updateScale ()
    {
        Transform3D t3d = new Transform3D();
        ftg.getTransform(t3d);
        t3d.setScale(scale);
        ftg.setTransform(t3d);

        if (frbg != null) { frbg.detach(); }
        frbg = makeFrameGraph();
        rtg.addChild(frbg);
    }
    public void editScale (float sca) 
    {
        scale += sca; 
        updateScale();
    }
    public void setScale (float sca) 
    {
        scale = sca; 
        updateScale();
    }

    protected BranchGroup makeFrameGraph ()
    {
        Appearance ap =  new Appearance();
        Material ma = new Material(
                new Color3f(0.0f, 0.0f, 0.0f), // ambient
                new Color3f(0.0f, 0.0f, 0.0f), // emissive
                new Color3f(0.0f, 0.0f, 0.0f), // diffuse
                new Color3f(0.0f, 0.0f, 0.0f), // specular
                0.0f);
        ap.setMaterial(ma);
        return makeFrameGraph(ap);
    }

    protected BranchGroup makeFrameGraph (Appearance ap, boolean wTex,
            float txw, float txh)
    {
        BranchGroup bg = new BranchGroup();
        bg.setCapability(BranchGroup.ALLOW_DETACH);
        Transform3D t3d;

        float w = dims.x*scale;
        float h = dims.y*scale;
        float d = depth;

        Geometry[] frame = {
            Util.rectMesh(w, d, -w/2.0f, 0.0f, h/2.0f, divs, wTex, txw, txh),
            Util.rectMesh(w, d, -w/2.0f, -d, h/2.0f, divs, wTex, txw, txh),
            Util.rectMesh(d, h, 0.0f, -h/2.0f, w/2.0f, divs, wTex, txw, txh),
            Util.rectMesh(d, h, -d, -h/2.0f, w/2.0f, divs, wTex, txw, txh)
        };
        TransformGroup[] frame_tgs = {
            new TransformGroup(),
            new TransformGroup(),
            new TransformGroup(),
            new TransformGroup()
        };

        t3d = new Transform3D();
        t3d.rotX((float) Math.PI/2);
        frame_tgs[0].setTransform(t3d);
        frame_tgs[0].addChild(new Shape3D(frame[0], ap));

        t3d = new Transform3D();
        t3d.rotX((float) -Math.PI/2);
        frame_tgs[1].setTransform(t3d);
        frame_tgs[1].addChild(new Shape3D(frame[1], ap));

        t3d = new Transform3D();
        t3d.rotY((float) -Math.PI/2);
        frame_tgs[2].setTransform(t3d);
        frame_tgs[2].addChild(new Shape3D(frame[2], ap));

        t3d = new Transform3D();
        t3d.rotY((float) Math.PI/2);
        frame_tgs[3].setTransform(t3d);
        frame_tgs[3].addChild(new Shape3D(frame[3], ap));

        BranchGroup paintingGraph = new BranchGroup();
        for (TransformGroup t: frame_tgs) { bg.addChild(t); }
        bg.setPickable(false);

        return bg;
    }

    protected BranchGroup makeFrameGraph (Appearance ap)
    {
        return makeFrameGraph(ap, false, 0.0f, 0.0f);
    }
}

