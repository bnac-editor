/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor.event;

import cl.uchile.dcc.bnac.Wall;

import javax.vecmath.Point2f;
import javax.vecmath.Point2i;

/**
 * An event which indicates an interaction with a wall has happened.
 */

public class WallEvent
{
    protected Wall wall;
    protected int idx;
    protected Point2f coords;
    protected Point2i pix;

    public WallEvent (Wall wall, int idx, Point2f coords, Point2i pix)
    {
        this.wall = wall;
        this.idx = idx;
        this.coords = new Point2f(coords);
        this.pix = new Point2i(pix);
    }

    public Wall getWall () { return wall; }
    public int getIndex () { return idx; }
    public Point2f getCoords () { return coords; }
    public Point2i getPix () { return pix; }
}

