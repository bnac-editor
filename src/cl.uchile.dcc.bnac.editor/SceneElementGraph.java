/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import javax.media.j3d.BranchGroup;

abstract class SceneElementGraph
{
    protected Project project;
    protected BranchGroup handle;

    public SceneElementGraph (Project project)
    {
        this.project = project;
        handle = new BranchGroup();
        handle.setCapability(BranchGroup.ALLOW_DETACH);
        handle.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        handle.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        handle.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
    }

    public BranchGroup getHandle () { return handle; }
    public void detach () { handle.detach(); }

    abstract public String getId ();
}

