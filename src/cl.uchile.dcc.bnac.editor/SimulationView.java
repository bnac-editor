/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Attribute;
import cl.uchile.dcc.bnac.Capture;
import cl.uchile.dcc.bnac.Door;
import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.PlacedCapture;
import cl.uchile.dcc.bnac.Wall;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.Geometry;
import javax.media.j3d.GraphicsConfigTemplate3D;
import javax.media.j3d.Locale;
import javax.media.j3d.Material;
import javax.media.j3d.PhysicalBody;
import javax.media.j3d.PhysicalEnvironment;
import javax.media.j3d.PickInfo;
import javax.media.j3d.PickRay;
import javax.media.j3d.PointLight;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.View;
import javax.media.j3d.ViewPlatform;
import javax.media.j3d.VirtualUniverse;
import javax.vecmath.Color3f;
import javax.vecmath.Point2f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

class SimulationView extends ProjectView
    implements KeyListener, MouseListener, ComponentListener
{
    /* J3D garbage */
    protected Canvas3D cv;
    protected VirtualUniverse u;
    protected Locale loc;
    protected BranchGroup vproot;
    protected TransformGroup vptg;
    protected ViewPlatform vp;
    protected View vi;
    protected BranchGroup scene;

    /* movement info */
    protected float angY;
    protected Vector3f lookDir;
    protected Vector3f position;

    HallGraph hg;
    String sid;

    public SimulationView (Project p, GraphicsConfiguration gc)
    {
        super(p);
        cv = new Canvas3D(gc.getDevice().getBestConfiguration(
                    new GraphicsConfigTemplate3D()));
        u = new VirtualUniverse();
        loc = new Locale(u);
        vproot = new BranchGroup();
        vptg = new TransformGroup();
        vp = new ViewPlatform();
        vi = new View();
        scene = new BranchGroup();

        lookDir = new Vector3f(0.0f, 0.0f, -1.0f);
        position = new Vector3f(1.0f, 1.70f, -1.0f);
        angY = 0.0f;

        buildUniverse();

        hg = project.getCurrentHallGraph();
        if (hg != null) { scene.addChild(hg.getHandle()); }
        loc.addBranchGraph(scene);

        addComponentListener(this);
        cv.addKeyListener(project);
        cv.addKeyListener(this);
        cv.addMouseListener(this);

        setPreferredSize(new Dimension(720, 480));

        setLayout(new BorderLayout());
        add(cv, BorderLayout.CENTER);
    }

    protected void buildUniverse ()
    {
        vptg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        vptg.addChild(vp);
        vproot.addChild(vptg);
        loc.addBranchGraph(vproot);

        Transform3D t3d = new Transform3D();
        t3d.rotY(angY);
        t3d.set(position);
        vptg.setTransform(t3d);

        vi.setPhysicalBody(new PhysicalBody());
        vi.setPhysicalEnvironment(new PhysicalEnvironment());
        vi.addCanvas3D(cv);
        vi.attachViewPlatform(vp);

        scene.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        scene.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        scene.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);

        AmbientLight al = new AmbientLight();
        al.setInfluencingBounds(new BoundingSphere(
                    new Point3d(0.0d, 0.0d, 0.0d), Double.MAX_VALUE));
        al.setColor(new Color3f(1.0f, 1.0f, 1.0f));
        scene.addChild(al);

        PointLight pl = new PointLight();
        pl.setInfluencingBounds(new BoundingSphere(
                    new Point3d(0.0d, 0.0d, 0.0d), Double.MAX_VALUE));
        pl.setPosition(1.0f, 1.6f, -1.0f);
        pl.setColor(new Color3f(1.0f, 1.0f, 1.0f));
        pl.setAttenuation(0.7f, 0.2f, 0.02f);
        scene.addChild(pl);
    }

    public void setHallGraph (HallGraph hg)
    {
        if (this.hg != null) { this.hg.detach(); }
        this.hg = hg;
        if (hg != null) { scene.addChild(hg.getHandle()); }
    }
    
    public void selectElement (String id)
    {
        sid = id;
        if (hg.getPortraitGraph(id) != null) {
            hg.getPortraitGraph(id).select();
        } else if (hg.getDoorGraph(id) != null) {
            hg.getDoorGraph(id).select();
        } else {
            sid = null;
        }
    }

    public void deselectElement (String id)
    {
        if (hg.getPortraitGraph(id) != null) {
            hg.getPortraitGraph(id).deselect();
        } else if (hg.getDoorGraph(id) != null) {
            hg.getDoorGraph(id).deselect();
        }
    }

    public void deselectAll ()
    {
        if (sid != null) { deselectElement(sid); }
        sid = null;
    }

    public void updateLookDirection ()
    {
        if (angY < -Math.PI) { angY += 2.0*Math.PI; }
        else if (angY > Math.PI) { angY -= 2.0*Math.PI; }
        if (Math.abs(angY - Math.PI/2.0) < 0.001) {
            angY = (float) (Math.PI/2.0);
            lookDir.set(-1.0f, 0.0f, 0.0f);
        } else if (Math.abs(angY + Math.PI/2.0) < 0.001) {
            angY = (float) (-Math.PI/2.0);
            lookDir.set(1.0f, 0.0f, 0.0f);
        } else if (Math.abs(angY + Math.PI) < 0.001) {
            angY = (float) Math.PI;
            lookDir.set(0.0f, 0.0f, 1.0f);
        } else if (Math.abs(angY - Math.PI) < 0.001) {
            angY = (float) Math.PI;
            lookDir.set(0.0f, 0.0f, 1.0f);
        } else {
            if (angY < Math.PI/2.0 && angY > -Math.PI/2.0) {
                lookDir.set((float) -Math.tan(angY), 0.0f, -1.0f);
            } else {
                lookDir.set((float) -Math.tan(-angY), 0.0f, 1.0f);
            }
            lookDir.normalize();
        }
    }

    public void updateView ()
    {
        Transform3D t3d = new Transform3D();
        t3d.rotY(angY);
        t3d.setTranslation(position);
        vptg.setTransform(t3d);
    }

    public float getAngleY () { return angY; }
    public Vector3f getLookDir () { return new Vector3f(lookDir); }
    public Vector3f position () { return new Vector3f(position); }
    public Vector2f position2f ()
    {
        return new Vector2f(position.x, -position.z);
    }

    public void keyPressed (KeyEvent e)
    {
        int kc = e.getKeyCode();
        if (kc == KeyEvent.VK_U || kc == KeyEvent.VK_I) {
            double ang = (kc == KeyEvent.VK_U) ? 5.0 : -5.0;
            angY += (float) Math.toRadians(ang);
            if (angY < -Math.PI) { angY += 2.0*Math.PI; }
            else if (angY > Math.PI) { angY -= 2.0*Math.PI; }
            updateLookDirection();
        } else if (kc == KeyEvent.VK_W || kc == KeyEvent.VK_S) {
            position.scaleAdd((kc==KeyEvent.VK_W) ? 0.25f : -0.25f,
                    lookDir, position);
        } else if (kc == KeyEvent.VK_A || kc == KeyEvent.VK_D) {
            Vector3f dir = new Vector3f();
            dir.cross(lookDir, new Vector3f(0.0f, 1.0f, 0.0f));
            dir.normalize();
            position.scaleAdd((kc==KeyEvent.VK_A) ? -0.25f : 0.25f,
                    dir, position);
        } else if (kc == KeyEvent.VK_R) {
            position.set(1.0f, 1.70f, -1.0f);
            lookDir.set(0.0f, 0.0f, -1.0f);
            angY = 0.0f;
        } else {
            if (sid == null) { return; }
            switch (kc) {
                case KeyEvent.VK_DELETE:
                    deselectElement(sid);
                    if (hg.getPortraitGraph(sid) != null) {
                        project.pushCommand(new RemovePortraitGraphCommand(
                                    project, hg, sid));
                    } else if (hg.getDoorGraph(sid) != null) {
                        project.pushCommand(new RemoveDoorGraphCommand(
                                    project, hg, sid));
                    }
                    sid = null;
                    return;
                case KeyEvent.VK_ESCAPE:
                    deselectAll();
                    return;
                case KeyEvent.VK_DOWN:
                    project.pushCommand(new MoveElem2DCommand(project, hg,
                                sid, 0.0f, -0.05f));
                    break;
                case KeyEvent.VK_UP:
                    project.pushCommand(new MoveElem2DCommand(project, hg,
                                sid, 0.0f, 0.05f));
                    break;
                case KeyEvent.VK_RIGHT:
                    project.pushCommand(new MoveElem2DCommand(project, hg,
                                sid, 0.05f, 0.0f));
                    break;
                case KeyEvent.VK_LEFT:
                    project.pushCommand(new MoveElem2DCommand(project, hg,
                                sid, -0.05f, 0.0f));
                    break;
                case KeyEvent.VK_PERIOD:
                    project.pushCommand(
                            new RotateElem2DCommand(project, hg, sid,
                                (float) -Math.toRadians(-5.0)));
                    break;
                case KeyEvent.VK_COMMA:
                    project.pushCommand(
                            new RotateElem2DCommand(project, hg, sid,
                                (float) -Math.toRadians(5.0)));
                    break;
                case KeyEvent.VK_EQUALS:
                    project.pushCommand(
                            new ScaleElem2DCommand(project, hg, sid, 0.07f));
                    break;
                case KeyEvent.VK_MINUS:
                    project.pushCommand(
                            new ScaleElem2DCommand(project, hg, sid, -0.07f));
                    break;
                default:
                    return;
            }

            return;
        }
        updateView();
        project.getPlotView().update();
    }

    public void keyReleased (KeyEvent e) { }
    public void keyTyped (KeyEvent e) { }

    public void mouseClicked (MouseEvent e)
    {
        Dimension d = cv.getSize();
        double ratioX = (1.0*e.getX())/d.width;
        double phi = Math.PI/4.0;
        double ratioY = (1.0*e.getY())/d.height;
        double theta = phi * d.height / d.width;
        double angPick = angY - phi*(ratioX-0.5);
        Vector3d dir;

        if (angPick > Math.PI) { angPick -= Math.PI*2.0; }
        else if (angPick < -Math.PI) { angPick += Math.PI*2.0; }

        if (Math.abs(angPick - Math.PI/2.0) < 0.00) {
            dir = new Vector3d(-1.0, 0.0, 0.0);
        } else if (Math.abs(angPick + Math.PI/2.0) < 0.00) {
            dir = new Vector3d(1.0, 0.0, 0.0);
        } else {
            if (angPick < Math.PI/2.0 && angPick > -Math.PI/2.0) {
                dir = new Vector3d(-Math.tan(angPick), 0.0, -1.0);
            } else {
                dir = new Vector3d(-Math.tan(-angPick), 0.0, 1.0);
            }
        }
        dir.normalize();
        dir.y = -Math.tan(theta*(ratioY-0.5));

        /* Check if it is horizonally within range, |θ| <= π/2 */
        if (Math.abs(theta*(ratioY-0.5)) > Math.PI/2.0) {
            deselectAll();
            return;
        }
        dir.normalize();
        BnacEditor.trace("pick dir: (%.02f,%.02f,%.02f)",
                dir.x, dir.y, dir.z);
        PickInfo pi = scene.pickClosest(
                PickInfo.PICK_BOUNDS,
                PickInfo.NODE,
                new PickRay(
                    new Point3d(position),
                    dir
                ));
        if (pi == null) {
            deselectAll();
        } else if (pi.getNode() != null) {
            String name = pi.getNode().getName();
            BnacEditor.trace("pick hit: %s", name);
            if (e.getClickCount() == 1) {
                if (sid != null && !sid.equals(name)) { 
                    deselectElement(sid);
                }
                if (sid == null || !sid.equals(name)) {
                    sid = name;
                    selectElement(sid);
                }
            } else if (e.getClickCount()==2 && hg.getDoorGraph(name)!=null) {
                String d2 =
                    project.getMuseumEnvironment().getConnection(name);

                if (d2 == null) {
                    BnacEditor.trace("%s has no link", name);
                    return;
                }

                BnacEditor.trace("%s -> %s", name, d2);

                deselectAll();

                Hall hall = null;
                for (Hall tmp: project.getMuseumEnvironment().hallArray()) {
                    if (tmp.getDoor(d2) != null) {
                        hall = tmp;
                        project.setCurrentHall(tmp.getId());
                        break;
                    }
                }

                if (hall == null) {
                    BnacEditor.error("trying to go to nonexistent hall");
                    return;
                }

                BnacEditor.trace("going to hall %s", hall.getId());

                Door door = hall.getDoor(d2);
                Wall wall = hall.getWall(hall.getParentWall(d2));

                float disp = door.getPosition().x;

                Vector2f v = new Vector2f();
                v.sub(wall.end2f(), wall.origin2f());
                v.normalize();

                Vector2f n2d = new Vector2f(v.y, -v.x);
                n2d.normalize();
                BnacEditor.trace("ori (%.02f,%.02f)", wall.origin2f().x,
                        wall.origin2f().y);
                BnacEditor.trace("vec (%.02f,%.02f)", v.x, v.y);
                BnacEditor.trace("nor (%.02f,%.02f)", n2d.x, n2d.y);

                Vector2f pos = new Vector2f();
                pos.scaleAdd(disp, v, wall.origin2f());
                n2d.scale(0.80f);
                pos.add(n2d);
                BnacEditor.trace("pos (%.02f,%.02f)", pos.x, pos.y);

                position.set(pos.x, 1.70f, -pos.y);

                if (Math.abs(n2d.x) < 0.001f) {
                    angY = (n2d.y>0.0f) ? 0.0f : (float) Math.PI;
                } else {
                    if (n2d.x > 0.0f) {
                        angY = -n2d.angle(new Vector2f(0.0f, 1.0f));
                    } else {
                        angY = n2d.angle(new Vector2f(0.0f, 1.0f));
                    }
                }

                BnacEditor.trace("@(%.02f,%.02f) %.02f",
                        pos.x, pos.y, (float) Math.toDegrees(angY));
                
                updateLookDirection();
                updateView();
            }
        }
    }

    public void mouseEntered (MouseEvent e) { }
    public void mouseExited (MouseEvent e) { }
    public void mousePressed (MouseEvent e) { }
    public void mouseReleased (MouseEvent e) { }

    public void componentResized (ComponentEvent e)
    {
        cv.setSize(getSize());
    }

    public void componentShown (ComponentEvent e) { }
    public void componentHidden (ComponentEvent e) { }
    public void componentMoved (ComponentEvent e) { }

    abstract public class Elem2DCommand extends ProjectCommand
    {
        protected HallGraph hg;
        protected String id;

        public Elem2DCommand (Project project, String name,
                HallGraph hg, String id)
        {
            super(project, name);
            this.hg = hg;
            this.id = id;
        }
    }

    public class MoveElem2DCommand extends Elem2DCommand
    {
        protected float dx;
        protected float dy;

        public MoveElem2DCommand (Project project,HallGraph hg, String id, 
                float dx, float dy)
        {
            super(project, "Move", hg, id);
            this.dx = dx;
            this.dy = dy;
        }

        public float getX () { return dx; }
        public float getY () { return dy; }

        public void execute ()
        {
            hg.getElem2DGraph(id).editPosition(new Point2f(dx, dy));
        } 
        public void undo ()
        {
            hg.getElem2DGraph(id).editPosition(new Point2f(-dx, -dy));
        }
    }

    public class RotateElem2DCommand extends Elem2DCommand
    {
        protected float ang;

        public RotateElem2DCommand (Project project, HallGraph hg,
                String id, float ang)
        {
            super(project, "Rotate", hg, id);
            this.ang = ang;
        }

        public float getAngle () { return ang; }

        public void execute () { hg.getElem2DGraph(id).editAngle(ang); }
        public void undo () { hg.getElem2DGraph(id).editAngle(-ang); }
    }

    public class ScaleElem2DCommand extends Elem2DCommand
    {
        protected float sca;

        public ScaleElem2DCommand (Project project, HallGraph hg,
                String id, float sca)
        {
            super(project, "Scale", hg, id);
            this.sca = sca;
        }

        public float getScale () { return sca; }

        public void execute () { hg.getElem2DGraph(id).editScale(sca); } 
        public void undo () { hg.getElem2DGraph(id).editScale(-sca); } 
    }

    public class RemovePortraitGraphCommand extends ProjectCommand
    {
        protected HallGraph hg;
        protected String id;
        protected int widx;
        protected PortraitGraph pg;

        public RemovePortraitGraphCommand (Project project, HallGraph hg,
                String id)
        {
            super(project, "REMOVE_PORTRAIT");
            this.hg = hg;
            this.id = id;
        }

        public void execute ()
        {
            widx = hg.getHall().getParentWall(id);
            pg = hg.removePortraitGraph(id);
        }

        public void undo () { hg.addPortraitGraph(widx, pg); }
    }

    public class RemoveDoorGraphCommand extends ProjectCommand
    {
        protected HallGraph hg;
        protected String id;
        protected int widx;
        protected DoorGraph pg;

        public RemoveDoorGraphCommand (Project project, HallGraph hg,
                String id)
        {
            super(project, "REMOVE_DOORS");
            this.hg = hg;
            this.id = id;
        }

        public void execute ()
        {
            widx = hg.getHall().getParentWall(id);
            pg = hg.removeDoorGraph(id);
        }

        public void undo () { hg.addDoorGraph(widx, pg); }
    }
}

