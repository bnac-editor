/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.Wall;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ListIterator;
import java.util.Locale;
import javax.swing.JLabel;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Vector2f;

class PlotView extends ProjectView
    implements MouseMotionListener
{
    protected HallGraph hg;
    protected PlotViewCanvas c;
    protected JLabel coords;

    public PlotView (Project p)
    {
        super(p);
        hg = p.getCurrentHallGraph();
        coords = new JLabel("0.00,0.00", JLabel.LEADING);
        c = new PlotViewCanvas(hg);

        setPrescence(
                project.getSimulationView().position2f(),
                project.getSimulationView().getAngleY());

        c.addMouseMotionListener(this);
        c.addKeyListener(project);

        setLayout(new BorderLayout(0, 2));
        add(coords, BorderLayout.SOUTH);
        add(c, BorderLayout.CENTER);
    }

    public void setHallGraph (HallGraph hg)
    {
        this.hg = hg;
        if (hg != null) {
            c.updateValues(hg);
            setPrescence(
                    project.getSimulationView().position2f(),
                    project.getSimulationView().getAngleY());
        }
        c.repaint();
    }

    public void update ()
    {
        c.updateValues();
        setPrescence(
                project.getSimulationView().position2f(),
                project.getSimulationView().getAngleY());
        c.repaint();
    }

    public void setPrescence (Vector2f pos, float ang)
    {
        c.setPrescence(pos, ang);
    }

    public void mouseDragged (MouseEvent e)
    {
        if (hg != null) {
            Point2f pos = c.pixToCoords(e.getX(), e.getY());
            coords.setText(String.format("%.02f,%.02f", pos.x, pos.y));
        }
    }

    public void mouseMoved (MouseEvent e)
    {
        if (hg != null) {
            Point2f pos;
            if (c.selC < 0) {
                pos = c.pixToCoords(e.getX(), e.getY());
            } else {
                pos = c.pixToCoords(c.xs[c.selC], c.ys[c.selC]);
            }
            coords.setText(String.format("%.02f,%.02f", pos.x, pos.y));
        }  else {
            coords.setText("");
        }
    }

    class PlotViewCanvas extends PlotCanvas
        implements MouseMotionListener, MouseListener, ComponentListener
    {
        protected HallGraph hg;

        protected int selW;
        protected int selC;
        protected Point2i drag;

        protected Point2i pos;
        protected float ang;

        protected int gr;
        protected Color hlColor;

        public PlotViewCanvas (HallGraph hg)
        {
            super((hg!=null)?hg.getHall():null, 300, 300, 25, 25);

            this.hg = hg;

            drag = new Point2i(-1, -1);
            selW = selC = -1;

            pos = new Point2i(-1, -1);
            ang = 0.0f;

            gr = 5;
            hlColor = Color.RED;

            addMouseMotionListener(this);
            addMouseListener(this);
            addComponentListener(this);
        }

        public void setPrescence (Vector2f pos, float ang)
        {
            if (hg == null) { return; }
            this.pos = coordsToPix(new Point2f(pos));
            this.ang = ang;
        }

        public void updateValues (HallGraph hg)
        {
            this.hg = hg;
            hall = hg.getHall();
            updateValues();
        }

        public void paint (Graphics g)
        {
            if (hall == null) {
                super.paint(g);
                return;
            }

            BufferedImage buf = new BufferedImage(getWidth(), getHeight(),
                        BufferedImage.TYPE_INT_RGB);
            Graphics g2 = buf.getGraphics();

            super.paint(g2);

            if (drag.x > -1 && drag.y > -1) { paintDragged(g2); }
            if (selW > -1) { paintHightlightedWall(g2); }
            paintGrips(g2);
            paintRuler(g2);
            paintPresence(g2);

            g.drawImage(buf, 0, 0, this);
        }

        protected void paintDragged (Graphics g)
        {
            g.drawOval(drag.x-gr, drag.y-gr,
                        2*gr+1, 2*gr+1);
            if (selC >= 0) {
                if (selC > 0) {
                    g.drawLine(xs[selC-1], ys[selC-1], drag.x, drag.y);
                } else {
                    g.drawLine(xs[xs.length-1], ys[ys.length-1],
                            drag.x, drag.y);
                }
                g.drawLine(xs[(selC+1)%xs.length], ys[(selC+1)%ys.length],
                        drag.x, drag.y);
            } else if (selW >= 0) {
                g.drawLine(xs[selW], ys[selW], drag.x, drag.y);
                g.drawLine(xs[(selW+1)%xs.length], ys[(selW+1)%ys.length],
                        drag.x, drag.y);
            }
        }

        protected void paintHightlightedWall (Graphics g)
        {
            g.setColor(hlColor);
            g.drawLine(xs[selW], ys[selW],
                    xs[(selW+1)%xs.length], ys[(selW+1)%ys.length]);
            g.setColor(fgColor);

        }

        protected void paintPresence (Graphics g)
        {
            if (pos.x < 0 || pos.y < 0) { return ; }
            float ang = -project.getSimulationView().getAngleY();
            Point2i pos = coordsToPix(new Point2f(
                        project.getSimulationView().position2f()));
            g.fillOval(pos.x-gr, pos.y-gr,
                        2*gr+1, 2*gr+1);
            g.drawLine(pos.x, pos.y,
                    (int) (pos.x + 15*Math.sin(ang)),
                    (int) (pos.y - 15*Math.cos(ang)));
        }

        protected void paintGrips (Graphics g)
        {
            for (int i=0; i<xs.length; ++i) {
                if (selC == i) { continue; }
                g.fillOval(xs[i]-gr, ys[i]-gr, 2*gr+1, 2*gr+1);
            }
            if (selC > -1) {
                g.setColor(hlColor);
                g.fillOval(xs[selC]-gr, ys[selC]-gr, 2*gr+1, 2*gr+1);
                g.setColor(fgColor);
            }
        }

        protected void paintRuler (Graphics g)
        {
            Point2f min = hall.min();
            Point2f dif = hall.dif();
            int OX = getHeight() - border.y/2;
            int OY = border.x/2;
            Point2i str = coordsToPix(min.x, min.y);
            Point2i end = coordsToPix(min.x+dif.x, min.y+dif.y);
            int u;

            g.drawLine(str.x, OX, end.x, OX);
            g.drawLine(OY, str.y, OY, end.y);

            for (int i=0; i<dif.x+1; ++i) {
                u = border.x + i*ppm;
                g.drawLine(u, OX-5, u, OX+5);
            }
            for (int i=0; i<dif.y+1; ++i) {
                u = getHeight() - border.y - i*ppm;
                g.drawLine(OY-5, u, OY+5, u);
            }
        }

        public void mousePressed (MouseEvent e)
        {
            if (e.getButton() == MouseEvent.BUTTON1) {
                if (selC > -1 || selW > -1) { drag.set(e.getX(), e.getY()); }
            }
        }

        public void mouseReleased (MouseEvent e)
        {
            Locale l = new Locale("en");
            Point2f d = pixToCoords(e.getX(), e.getY());
            d.set(
                    Float.parseFloat(String.format(l, "%.02f", d.x)),
                    Float.parseFloat(String.format(l, "%.02f", d.y)));
            if (selC > -1) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    project.pushCommand(new MoveCornerCommand(
                                project, hg, selC, d));
                } else if (e.getButton() == MouseEvent.BUTTON3) {
                    project.pushCommand(new RemoveCornerCommand(
                                project, hg, selC));
                }
                selC = -1;
                repaint();
            } else if (selW > -1) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    project.pushCommand(new AddCornerCommand(
                                project, hg, selW+1, d));
                }
                selW = -1;
                repaint();
            }

            drag.set(-1, -1);
        }

        public void mouseDragged (MouseEvent e)
        {
            if (selC > -1 || selW > -1) {
                drag.set(e.getX(), e.getY());
                repaint();
            }
        }

        public void mouseMoved (MouseEvent e)
        {
            if (hall == null) { return; }

            boolean change = false;
            Point2f pos = pixToCoords(e.getX(), e.getY());
            Point2f[] cs = hall.cornerArray();

            if (selC < 0) {
                for (int i=0; i<cs.length; ++i) {
                    if (cs[i].distance(pos) < 0.18f) {
                        selW = -1;
                        selC = i;
                        selW = -1;
                        change = true;
                        break;
                    }
                }
            } else if (cs[selC].distance(pos) >= 0.18f) {
                selC = -1;
                change = true;
            }

            if (selC < 0) {
                if (selW < 0) {
                    for (int i=0; i<cs.length; ++i) {
                        if (Util.isNearLine(cs[i], cs[(i+1)%cs.length], pos,
                                    0.1f)) {
                            selW = i;
                            change = true;
                            break;
                        }
                    }
                } else if (!Util.isNearLine(cs[selW], cs[(selW+1)%cs.length],
                            pos, 0.1f)) {
                    selW = -1;
                    change = true;
                }
            }

            if (change) {
                if (selC > -1 || selW > -1) {
                    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                } else {
                    setCursor(Cursor.getDefaultCursor());
                }

                repaint();
            }
        }

        public void mouseClicked (MouseEvent e) { }
        public void mouseEntered (MouseEvent e) { }
        public void mouseExited (MouseEvent e) { }

        public void componentResized (ComponentEvent e)
        {
            max.set(getWidth()-border.x*2, getHeight()-border.y*2);
            updateValues();
        }

        public void componentHidden (ComponentEvent e) { }
        public void componentMoved (ComponentEvent e) { }
        public void componentShown (ComponentEvent e) { }
    }

    abstract public class HallCommand extends ProjectCommand
    {
        protected HallGraph hg;
        protected int idx;

        public HallCommand (Project project, String name, HallGraph hg,
                int idx)
        {
            super(project, name);
            this.hg = hg;
            this.idx = idx;
        }

        public void callUpdates ()
        {
            project.getSimulationView().deselectAll();
            project.getHallView().updateHallBox(hg.getId());
            if (project.getCurrentHallGraph().getId().equals(hg.getId())) {
                project.getPlotView().update();
            }
        }

        public void execute ()
        {
            realExecute();
            callUpdates();
        }

        public void undo ()
        {
            realUndo();
            callUpdates();
        }

        abstract protected void realExecute();
        abstract protected void realUndo();
    }

    public class MoveCornerCommand extends HallCommand
    {
        protected Point2f orig;
        protected Point2f dst;

        public MoveCornerCommand (Project project, HallGraph hg, int idx,
                Point2f dst)
        {
            super(project, "MOVE_CORNER", hg, idx);
            BnacEditor.debug("move corner %d (%.02f,%.02f)", idx,
                    dst.x, dst.y);

            orig = hg.getHall().getCorner(idx);
            this.dst = new Point2f(dst);
        }

        protected void realExecute () { hg.moveCorner(idx, dst); }

        protected void realUndo () { hg.moveCorner(idx, orig); }
    }

    public class AddCornerCommand extends HallCommand
    {
        protected Point2f p;

        public AddCornerCommand (Project project, HallGraph hg, int idx,
                Point2f p)
        {
            super(project, "ADD", hg, idx);
            BnacEditor.debug("add corner %d (%.02f,%.02f)", idx, p.x, p.y);
            this.p = new Point2f(p);
        }

        protected void realExecute () { hg.addCorner(idx, p); }

        protected void realUndo ()
        {
            WallGraph wg = hg.getWallGraph(idx-1);
            WallGraph next = hg.getWallGraph(idx);
            float d = wg.getWall().width();
            hg.removeCorner(idx);
            wg.getWall().setEnd(next.getWall().end2f());
            hg.setWall(idx-1, wg);

            PortraitGraph[] pgs = next.portraitGraphArray();
            DoorGraph[] dgs = next.doorGraphArray();
            for (PortraitGraph pg: pgs) {
                pg.detach();
                pg.deselect();
                pg.editPosition(new Point2f(d, 0.0f));
                wg.addPortraitGraph(pg);
            }
            for (DoorGraph dg: dgs) {
                dg.detach();
                dg.deselect();
                dg.editPosition(new Point2f(d, 0.0f));
                wg.addDoorGraph(dg);
            }

            wg.update();
        }
    }

    public class RemoveCornerCommand extends HallCommand
    {
        protected WallGraph[] wgs;

        public RemoveCornerCommand (Project project, HallGraph hg, int idx)
        {
            super(project, "DELETE", hg, idx);
            BnacEditor.debug("remove corner %d", idx);
        }

        protected void realExecute () { wgs = hg.removeCorner(idx); }

        protected void realUndo ()
        {
            hg.addCorner(idx, wgs[0].getWall().end2f());
            hg.setWall((idx>0)?idx-1:hg.size()-1, wgs[0]);
            hg.setWall(idx, wgs[1]);
        }
    }
}

