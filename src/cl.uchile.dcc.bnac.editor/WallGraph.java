/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.PlacedCapture;
import cl.uchile.dcc.bnac.Wall;
import cl.uchile.dcc.bnac.Door;

import java.util.Hashtable;

import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Geometry;
import javax.media.j3d.Material;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;

import javax.vecmath.Color3f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

public class WallGraph extends SceneElementGraph
{
    protected Wall wall;
    String id;
    protected float height;
    protected int divs;
    protected Hashtable<String, PortraitGraph> pgs;
    protected Hashtable<String, DoorGraph> doors;
    protected TransformGroup tg;

    public WallGraph (Project project, Wall wall, float height, String id)
    {
        super(project);
        this.wall = wall;
        this.height = height;
        this.id = id;
        divs = 50;
        pgs = new Hashtable<String, PortraitGraph>();
        doors = new Hashtable<String, DoorGraph>();

        for (PlacedCapture pc: wall.placedCaptureArray()) {
            pgs.put(pc.getId(), new PortraitGraph(project, pc));
        }
        for (Door d: wall.doorArray()) {
            doors.put(d.getId(), new DoorGraph(project, d));
        }

        buildGraph();
    }

    public void update ()
    {
        handle.removeAllChildren();

        for (PortraitGraph pg: pgs.values()) { pg.detach(); }
        for (DoorGraph dg: doors.values()) { dg.detach(); }

        pgs.clear();
        doors.clear();

        for (PlacedCapture pc: wall.placedCaptureArray()) {
            pgs.put(pc.getId(), new PortraitGraph(project, pc));
        }
        for (Door d: wall.doorArray()) {
            doors.put(d.getId(), new DoorGraph(project, d));
        }

        buildGraph();
    }

    protected void buildGraph ()
    {
        Geometry geom = Util.rectMesh(
                wall.width(), height, 0.0f, 0.0f, 0.0f, divs); 

        Appearance ap = new Appearance();
        Material mat = new Material(
                new Color3f(0.2f, 0.19f, 0.16f), // ambient
                new Color3f(0.0f, 0.0f, 0.0f), // emissive
                new Color3f(1.0f, 0.93f, 0.80f), // diffuse
                new Color3f(0.0f, 0.0f, 0.0f), // specular
                0.0f);
        ap.setMaterial(mat);

        BranchGroup dummy = new BranchGroup();
        dummy.setCapability(BranchGroup.ALLOW_DETACH);
        tg = new TransformGroup();
        tg.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        tg.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
        Transform3D t3d = new Transform3D();

        Vector2f disp = new Vector2f();
        disp.sub(wall.end2f(), wall.origin2f());

        t3d.rotY(Util.angle(disp));
        t3d.setTranslation(new Vector3f(wall.origin3f()));
        tg.setTransform(t3d);

        Shape3D wallShape = new Shape3D(geom, ap);
        wallShape.setPickable(false);
        tg.addChild(wallShape);

        for (PortraitGraph pg: pgs.values()) { tg.addChild(pg.getHandle()); }
        for (DoorGraph dg: doors.values()) { tg.addChild(dg.getHandle()); }

        dummy.addChild(tg);
        handle.addChild(dummy);
    }

    public void setHeight (float height)
    {
        this.height = height;
        update();
    }

    public void addPortraitGraph (PortraitGraph pg)
    {
        wall.addPlacedCapture(pg.pcap);
        pgs.put(pg.pcap.getId(), pg);
        tg.addChild(pg.getHandle());
        for (String pgid: pgs.keySet()) { BnacEditor.trace("pg: %s", pgid); }
    }

    public PortraitGraph removePortraitGraph (String id)
    {
        PortraitGraph pg = pgs.get(id);
        if (pg != null) {
            pg.detach();
            pgs.remove(id);
            wall.removePlacedCapture(id);
        }
        for (String pgid: pgs.keySet()) { BnacEditor.trace("pg: %s", pgid); }
        return pg;
    }

    public PortraitGraph[] portraitGraphArray ()
    {
        return pgs.values().toArray(new PortraitGraph[0]);
    }

    public PortraitGraph getPortraitGraph (String id)
    {
        return pgs.get(id);
    }

    public void addDoorGraph (DoorGraph dg)
    {
        wall.addDoor(dg.getDoor());
        doors.put(dg.getId(), dg);
        tg.addChild(dg.getHandle());
    }

    public void removeDoor (String id)
    {
        DoorGraph dg = doors.get(id);
        if (dg != null) {
            dg.detach();
            pgs.remove(dg);
            wall.removeDoor(id);
        }
    }

    public DoorGraph[] doorGraphArray ()
    {
        return doors.values().toArray(new DoorGraph[0]);
    }

    public DoorGraph getDoorGraph (String id)
    {
        return doors.get(id);
    }

    public Wall getWall () { return wall; }

    public void setId (String id) { this.id = id; }
    public String getId () { return id; }
}

