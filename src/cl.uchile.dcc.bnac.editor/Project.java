/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.*;

import java.util.Stack;

import java.io.File;
import java.io.IOException;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Action;
import javax.swing.JFrame;

import javax.vecmath.Point2f;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Project
    implements WindowListener, KeyListener
{
    protected File root;
    protected MuseumEnvironment me;
    protected InfoSet info;
    protected HallGraph curhg;
    protected Stack<ProjectCommand> cmdUndo;
    protected Stack<ProjectCommand> cmdRedo;

    protected SimulationView simv;
    protected PlotView plotv;
    protected CaptureView capv;
    protected HallView hallv;
    protected NavigationView navv;
    protected RouteView rotv;

    protected JFrame simf;
    protected JFrame pltf;
    protected JFrame capf;
    protected JFrame half;
    protected JFrame navf;
    protected JFrame rotf;

    protected Project (File root, MuseumEnvironment me)
    {
        this.root = root;
        this.me = me;
        info = new InfoSet();
        curhg = null;
        cmdUndo = new Stack<ProjectCommand>();
        cmdRedo = new Stack<ProjectCommand>();

        initViews();

        if (me.hallArray().length > 0) {
            setCurrentHall(me.hallArray()[0].getId());
        }
    }

    protected void initViews ()
    {
        String[] strs = {
            "SIMULATIONV", "PLOTV", "CAPTUREV", "HALLV", "NAVIGATIONV",
            "ROUTEV"
        };
        JFrame[] jfs = new JFrame[strs.length];

        for (int i=0; i<jfs.length; ++i) {
            jfs[i] = new JFrame(Util.gs(strs[i]));
            jfs[i].setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            jfs[i].addWindowListener(this);
        }

        simv = new SimulationView(this, jfs[0].getGraphicsConfiguration());
        simf = jfs[0];
        simf.add(simv);
        simf.pack();

        plotv = new PlotView(this);
        pltf = jfs[1];
        pltf.add(plotv);
        pltf.pack();

        capv = new CaptureView(this);
        capf = jfs[2];
        capf.add(capv);
        capf.pack();

        hallv = new HallView(this);
        half = jfs[3];
        half.add(hallv);
        half.pack();

        navv = new NavigationView(this);
        navf = jfs[4];
        navf.add(navv);
        navf.pack();

        rotv = new RouteView(this);
        rotf = jfs[5];
        rotf.add(rotv);
        rotf.pack();
    }

    protected Project (String path)
        throws IOException, SAXException, ParserConfigurationException
    {
        this(new File(path),
                MuseumEnvironment.fromFile(
                    new File(path + System.getProperty("file.separator") +
                        "project.xml")));
        File infofile = new File(path, "info.xml");
        if (infofile.exists()) {
            info = InfoSet.fromFile(infofile);
        } else {
            info = new InfoSet();
        }
    }

    protected Project (MuseumEnvironment mm)
    {
        this(null, mm);
    }

    public void pushCommand (ProjectCommand pc)
    {
        BnacEditor.debug("push %s", pc.getName());
        pc.execute();
        cmdUndo.push(pc);
        cmdRedo.clear();
        updateActions();
    }

    public void undo ()
    {
        if (cmdUndo.size() > 0) {
            BnacEditor.debug("undo %s", cmdUndo.peek().getName());
            cmdUndo.peek().undo();
            cmdRedo.push(cmdUndo.pop());
            updateActions();
        }
    }

    public void redo ()
    {
        if (cmdRedo.size() > 0) {
            BnacEditor.debug("redo %s", cmdRedo.peek().getName());
            cmdRedo.peek().execute();
            cmdUndo.push(cmdRedo.pop());
            updateActions();
        }
    }

    public boolean close () { return true; }

    public String toString () { return me.get("Title"); }

    public HallGraph getCurrentHallGraph () { return curhg; }
    public void setCurrentHall (String id)
    {
        curhg = (id!=null) ? hallv.getHallGraph(id) : null;
        simv.setHallGraph(curhg);
        plotv.setHallGraph(curhg);
        hallv.setSelectedHallBox(id);
    }

    public void addObject (CObject obj)
    {
        me.addObject(obj);
    }

    public void addCapture (Capture cap)
    {
        me.addCapture(cap);
        capv.addCapture(cap);
        if (me.captureArray().length == 1) { capf.pack(); }
    }

    public void addHallGraph (HallGraph hg)
    {
        me.addHall(hg.getHall());
        hallv.addHallGraph(hg);
        if (me.hallArray().length == 1) { half.pack(); }
    }
    public HallGraph removeHallGraph (String id)
    {
        Hall hall = (id!=null) ? me.removeHall(id) : null;
        if (hall == null) { return null; }
        HallGraph hg = hallv.removeHallGraph(id);
        if (curhg.getId().equals(id)) {
            if (me.hallArray().length > 0) {
                setCurrentHall(me.hallArray()[0].getId());
            } else {
                setCurrentHall(null);
            }
        }
        return hg;
    }

    public String getRootPath ()
    {
        return (root!=null) ? root.getAbsolutePath() : null;
    }
    public void setRootPath (String path)
    {
        root = new File(path);
    }

    public void save () throws IOException
    {
        File xml;

        xml = new File(root, "project.xml");
        MemlWriter.write(me, xml);

        xml = new File(root, "info.xml");
        InfoWriter.write(info, xml);

        BnacEditor.info("project %s saved to %s", toString(),
                root.getAbsolutePath());
    }

    public MuseumEnvironment getMuseumEnvironment () { return me; }

    static public Project fromPath (String path)
        throws IOException, SAXException, ParserConfigurationException
    {
        return new Project(path);
    }

    public SimulationView getSimulationView () { return simv; }
    public PlotView getPlotView () { return plotv; }
    public CaptureView getCaptureView () { return capv; }
    public HallView getHallView () { return hallv; }
    public NavigationView getNavigationView () { return navv; }

    public JFrame getNavigationFrame () { return navf; }

    public void setSimulationViewVisible (boolean b) { simf.setVisible(b); }
    public void setPlotViewVisible (boolean b) { pltf.setVisible(b); }
    public void setCaptureViewVisible (boolean b) { capf.setVisible(b); }
    public void setHallViewVisible (boolean b) { half.setVisible(b); }
    public void setNavigationViewVisible (boolean b) { navf.setVisible(b); }
    public void setRouteViewVisible (boolean b) { rotf.setVisible(b); }

    static public Project createProject (File root, String title)
    {
        return new Project(root, new MuseumEnvironment(title));
    }

    public void addPlacedCapture (Capture cap)
    {
        /*
        curHall.getWall(0).addPlacedCapture(
                new PlacedCapture(
                    String.format("newcap-%d",
                        (int) System.currentTimeMillis()),
                    cap));
                    */
    }

    public void activate () { updateActions(); }

    protected void updateActions ()
    {
        Action undo = BnacEditor.getUndoAction();
        Action redo = BnacEditor.getRedoAction();

        if (cmdUndo.size() == 0) {
            undo.setEnabled(false);
            undo.putValue(Action.SHORT_DESCRIPTION, Util.gs("UNDO_SD"));
        } else {
            undo.setEnabled(true);
            undo.putValue(Action.SHORT_DESCRIPTION, String.format("%s (%s)",
                        Util.gs("UNDO_SD"),
                        Util.gs(cmdUndo.peek().getName().toUpperCase())));
        }

        if (cmdRedo.size() == 0) {
            redo.setEnabled(false);
            redo.putValue(Action.SHORT_DESCRIPTION, Util.gs("REDO_SD"));
        } else {
            redo.setEnabled(true);
            redo.putValue(Action.SHORT_DESCRIPTION, String.format("%s (%s)",
                        Util.gs("REDO_SD"),
                        Util.gs(cmdRedo.peek().getName().toUpperCase())));
        }
    }

    public void keyPressed (KeyEvent e)
    {
        int kc = e.getKeyCode();
        boolean ctrl = e.isControlDown();
        boolean shft = e.isShiftDown();
        boolean alt = e.isAltDown();

        if (ctrl && !shft && !alt && kc == KeyEvent.VK_Z) { undo(); }
        if (ctrl && shft && !alt && kc == KeyEvent.VK_Z) { redo(); }
        if (!ctrl && !shft && alt) {
            JFrame jf = null;
            switch (kc) {
                case KeyEvent.VK_1:
                    jf = simf;
                    break;
                case KeyEvent.VK_2:
                    jf = capf;
                    break;
                case KeyEvent.VK_3:
                    jf = half;
                    break;
                case KeyEvent.VK_4:
                    jf = pltf;
                    break;
                case KeyEvent.VK_5:
                    jf = navf;
                    break;
                case KeyEvent.VK_6:
                    jf = rotf;
                    break;
                default:
                    break;
            }
            if (jf != null) {
                jf.setVisible(true);
                jf.toFront();
            }
        }
    }

    public void keyReleased (KeyEvent e) { }
    public void keyTyped (KeyEvent e) { }

    public void windowActivated (WindowEvent e)
    {
        if (BnacEditor.autoFocus()) { BnacEditor.selectProject(this); }
    }

    public void windowClosing (WindowEvent e) { }
    public void windowClosed (WindowEvent e) { }
    public void windowDeactivated (WindowEvent e) { }
    public void windowDeiconified (WindowEvent e) { }
    public void windowIconified (WindowEvent e) { }
    public void windowOpened (WindowEvent e) { }

    public InfoSet getInfoSet () { return info; }
}

