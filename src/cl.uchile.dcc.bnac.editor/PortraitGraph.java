/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.PlacedCapture;
import cl.uchile.dcc.bnac.Capture2D;
import cl.uchile.dcc.bnac.Attribute;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingBox;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Geometry;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.Material;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture2D;
import javax.media.j3d.Texture;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;

import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

class PortraitGraph extends Element2DGraph
{
    protected PlacedCapture pcap;
    protected BranchGroup canvasNrm;
    protected BranchGroup canvasSel;
    protected boolean selected;

    public PortraitGraph (Project project, PlacedCapture pc)
    {
        super(project);
        pcap = pc;
        selected = false;

        Capture2D c = (Capture2D) pcap.capture;
        dims.set(c.width(), c.height());
        pos.set(pcap.t);
        angle = pcap.angle;
        scale = pcap.scale;

        frbg = makeFrameGraph();
        rtg.addChild(frbg);
        rtg.addChild(ftg);
        handle.addChild(rtg);

        buildFace();
        updatePosition();
        updateDimensions();
        updateAngle();
        updateScale();
    }

    protected void buildFace ()
    {
        try {
            Capture2D c = (Capture2D) pcap.capture;
            
            BufferedImage img;
            img = ImageIO.read(new File(
                        project.getRootPath() +
                        System.getProperty("file.separator") +
                        c.get(Attribute.Resource)));
            BufferedImage pimg = Util.padToPowerOf2(img);
            int w = img.getWidth();
            int h = img.getHeight();
            int pw = pimg.getWidth();
            int ph = pimg.getHeight();

            BnacEditor.trace("tex %dx%d", w, h);
            Texture2D tex = new Texture2D(Texture.BASE_LEVEL, Texture.RGB,
                    pw, ph);
            tex.setBoundaryModeS(Texture.CLAMP);
            tex.setBoundaryModeT(Texture.CLAMP);
            tex.setImage(0, new ImageComponent2D(
                        ImageComponent.FORMAT_RGB, pimg));
            TextureAttributes tattr = new TextureAttributes();
            tattr.setTextureMode(TextureAttributes.MODULATE);

            Shape3D canvas;
            Appearance ap;
            Geometry geom =
                Util.rectMesh(c.width(), c.height(),
                        -c.width()/2.0f, -c.height()/2.0f,
                        0.0f, divs, true, ((float) w)/pw, ((float) h)/ph);

            canvasNrm = new BranchGroup();
            canvasNrm.setCapability(BranchGroup.ALLOW_DETACH);
            canvasSel = new BranchGroup();
            canvasSel.setCapability(BranchGroup.ALLOW_DETACH);

            ap = new Appearance();
            ap.setTexture(tex);
            ap.setTextureAttributes(tattr);
            ap.setMaterial(new Material(
                        new Color3f(0.2f, 0.2f, 0.2f), // ambient
                        new Color3f(0.0f, 0.0f, 0.0f), // emissive
                        new Color3f(0.8f, 0.8f, 0.8f), // diffuse
                        new Color3f(0.0f, 0.0f, 0.0f), // specular
                        0.0f));
            canvas = new Shape3D(geom, ap);
            canvas.setPickable(true);
            canvas.setBounds(new BoundingBox(
                        new Point3d(0.0, 0.0, 0.0),
                        new Point3d(c.width(), c.height(), -depth)));
            canvas.setName(pcap.getId());
            canvasNrm.addChild(canvas);

            ap = new Appearance();
            ap.setTexture(tex);
            ap.setTextureAttributes(tattr);
            ap.setMaterial(new Material(
                        new Color3f(0.0f, 0.0f, 0.0f), // ambient
                        new Color3f(0.8f, 0.0f, 0.0f), // emissive
                        new Color3f(0.0f, 0.0f, 0.0f), // diffuse
                        new Color3f(0.0f, 0.0f, 0.0f), // specular
                        0.0f));
            TransparencyAttributes tra = new TransparencyAttributes(
                    TransparencyAttributes.FASTEST, 0.5f);
            ap.setTransparencyAttributes(tra);

            canvas = new Shape3D(geom, ap);
            canvas.setPickable(true);
            canvas.setBounds(new BoundingBox(
                        new Point3d(0.0, 0.0, 0.0),
                        new Point3d(c.width(), c.height(), -depth)));
            canvas.setName(pcap.getId());
            canvasSel.addChild(canvas);

            ftg.addChild(canvasNrm);
        } catch (Exception e) {
            BnacEditor.error("when rendering %s", e, pcap.capture.getId());
        }
    }

    public void select ()
    {
        if (selected) { return; }
        BnacEditor.trace("selected %s", getId());
        selected = true;
        canvasNrm.detach();
        ftg.addChild(canvasSel);
    }

    public void deselect ()
    {
        if (!selected) { return; }
        BnacEditor.trace("deselected %s", getId());
        selected = false;
        canvasSel.detach();
        ftg.addChild(canvasNrm);
    }

    public PlacedCapture getPlacedCapture () { return pcap; }

    public String getId () { return pcap.getId(); }

    public void updatePosition ()
    {
        super.updatePosition();
        pcap.t.set(pos);
    }
    public void updateAngle ()
    {
        super.updateAngle();
        pcap.angle = angle;
    }
    public void updateScale ()
    {
        super.updateScale();
        pcap.scale = scale;
    }
}

