/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.Link;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.vecmath.Point2f;

public class HallView extends ProjectView
    implements ActionListener
{
    protected ArrayList<HallBox> hallList;
    protected HallBox curHallGraph;

    protected Box box_list;

    public HallView (Project project)
    {
        super(project);
        hallList = new ArrayList<HallBox>();
        box_list = new Box(BoxLayout.PAGE_AXIS);

        HallBox hb;
        for (Hall hall: project.getMuseumEnvironment().hallArray()) {
            hb = new HallBox(new HallGraph(project, hall));
            hallList.add(hb);
        }

        JButton nh = new JButton(Util.gs("NEW_HALL"));
        nh.addActionListener(this);

        for (HallBox hbb: hallList) { box_list.add(hbb); }

        setLayout(new BorderLayout(0, 2));
        add(nh, BorderLayout.NORTH);
        add(new JScrollPane(box_list,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                BorderLayout.CENTER);
    }

    public void addHallGraph (HallGraph hall)
    {
        HallBox hb = new HallBox(hall);
        hallList.add(hb);
        box_list.add(hb);
        ((JScrollPane) getComponent(1)).updateUI();
    }

    public HallGraph removeHallGraph (String id)
    {
        HallBox hb = null;
        for (HallBox hbb: hallList) {
            if (hbb.getHallId().equals(id)) {
                hb = hbb;
                hallList.remove(hb);
                break;
            }
        }
        if (hb != null) {
            box_list.remove(hb);
            ((JScrollPane) getComponent(1)).updateUI();
            return hb.getHallGraph();
        }
        return null;
    }

    public HallGraph getHallGraph (String id)
    {
        for (HallBox hbb: hallList) {
            if (hbb.getHallId().equals(id)) { return hbb.getHallGraph(); }
        }
        return null;
    }

    public void updateHallBox (String id)
    {
        for (HallBox hbb: hallList) {
            if (hbb.getHallId().equals(id)) {
                hbb.update();
                break;
            }
        }
    }

    public void setSelectedHallBox (String id)
    {
        if (curHallGraph != null) { curHallGraph.setSelected(false); }
        for (HallBox hb: hallList) {
            if (hb.getHallId().equals(id)) {
                hb.setSelected(true);
                curHallGraph = hb;
                return;
            }
        }
    }

    public void actionPerformed (ActionEvent ae)
    {
        project.pushCommand(new NewHallGraphCommand(project));
    }

    abstract class HallGraphCommand extends ProjectCommand
    {
        public HallGraph hg;

        public HallGraphCommand (Project project, String name, HallGraph hg)
        {
            super(project, name);
            this.hg = hg;
        }
    }

    class NewHallGraphCommand extends HallGraphCommand
    {
        public NewHallGraphCommand (Project project)
        {
            super(project, "NEW_HALL", new HallGraph(project,
                        String.format("hall_%02d",
                            project.getMuseumEnvironment().hallArray().length)
                        ));
            hg.getHall().addStartingPoint(
                    String.format("sp1_%09d", System.currentTimeMillis()),
                    new Point2f(hg.getHall().centre2f(false)), 0.0f);
        }

        public void execute ()
        {
            project.addHallGraph(hg);
            project.setCurrentHall(hg.getId());
        }

        public void undo ()
        {
            project.removeHallGraph(hg.getId());
        }
    }

    class RemoveHallGraphCommand extends ProjectCommand
    {
        protected String id;
        protected HallGraph hg;

        protected ArrayList<Link> ls;
        protected ArrayList<DoorGraph> dgs2;
        protected ArrayList<Integer> widxs2;
        protected ArrayList<HallGraph> hgs2;

        public RemoveHallGraphCommand (Project project, String id)
        {
            super(project, "REMOVE_HALL");
            this.id = id;
            hg = project.getHallView().getHallGraph(id);

            ls = new ArrayList<Link>();
            dgs2 = new ArrayList<DoorGraph>();
            widxs2 = new ArrayList<Integer>();
            hgs2 = new ArrayList<HallGraph>();

            HallGraph tmp;
            for (WallGraph wg: hg.wallGraphArray()) {
                for (DoorGraph dg: wg.doorGraphArray()) {
                    BnacEditor.trace("for DoorGraph %s", dg.getId());

                    for (Link l: project.getMuseumEnvironment().linkArray()) {
                        if (l.d1.equals(dg.getId())) {
                            ls.add(l);
                            BnacEditor.trace("added Link %s", l.getId());

                            tmp = project.getHallView().getHallGraph(
                                    project.getMuseumEnvironment().
                                    getDoorParentHall(l.d2).getId());
                            dgs2.add(tmp.getDoorGraph(l.d2));
                            BnacEditor.trace("added DoorGraph2 %s",
                                    tmp.getDoorGraph(l.d2).getId());
                            hgs2.add(tmp);
                            BnacEditor.trace("added HallGraph2 %s",
                                    tmp.getId());
                            widxs2.add(new Integer(tmp.getHall().
                                        getParentWall(l.d2)));
                            BnacEditor.trace("added index %d",
                                    new Integer(tmp.getHall().
                                        getParentWall(l.d2)));
                        } else if (l.d2.equals(dg.getId())) {
                            ls.add(l);
                            BnacEditor.trace("added Link %s", l.getId());

                            tmp = project.getHallView().getHallGraph(
                                    project.getMuseumEnvironment().
                                    getDoorParentHall(l.d1).getId());
                            dgs2.add(tmp.getDoorGraph(l.d1));
                            BnacEditor.trace("added DoorGraph2 %s",
                                    tmp.getDoorGraph(l.d1).getId());
                            hgs2.add(tmp);
                            BnacEditor.trace("added HallGraph2 %s",
                                    tmp.getId());
                            widxs2.add(new Integer(tmp.getHall().
                                        getParentWall(l.d1)));
                            BnacEditor.trace("added index %d",
                                    tmp.getHall().getParentWall(l.d1));
                        }
                    }
                }
            }
        }

        public void execute ()
        {
            for (Link l: ls) {
                project.getMuseumEnvironment().removeLink(l.getId());
                project.getNavigationView().removeLink(l.getId());
            }
            for (int i=0; i<dgs2.size(); ++i) {
                hgs2.get(i).removeDoorGraph(dgs2.get(i).getId());
            }
            project.removeHallGraph(id);
        }

        public void undo ()
        {
            project.addHallGraph(hg);
            for (int i=0; i<dgs2.size(); ++i) {
                hgs2.get(i).addDoorGraph(widxs2.get(i), dgs2.get(i));
            }
            for (Link l: ls) {
                project.getMuseumEnvironment().addLink(l);
                project.getNavigationView().addLink(l);
            }
        }
    }

    class EditHallCommand extends ProjectCommand
    {
        protected HallGraph hg;
        protected String oldName;
        protected String newName;
        protected float oldHeight;
        protected float newHeight;

        public EditHallCommand (Project project, HallGraph hg, String newName,
                float newHeight)
        {
            super(project, "EDIT_HALL");
            this.hg = hg;
            this.newName = newName;
            this.newHeight = newHeight;
            oldName = hg.getHall().getName();
            oldHeight = hg.getHall().getHeight();
        }

        public void execute ()
        {
            if (Math.abs(newHeight-oldHeight) > 0.001f) {
                hg.setHeight(newHeight);
                hg.update();
            }
            if (!newName.equals(oldName)) { hg.getHall().setName(newName); }
            project.getHallView().updateHallBox(hg.getId());
        }

        public void undo ()
        {
            if (Math.abs(newHeight-oldHeight) > 0.001f) {
                hg.setHeight(oldHeight);
                hg.update();
            }
            if (!newName.equals(oldName)) { hg.getHall().setName(oldName); }
            project.getHallView().updateHallBox(hg.getId());
        }
    }

    class HallBox extends JPanel implements ActionListener
    {
        public HallGraph hg;

        protected JLabel lb_img;
        protected JTextField tf_nam;
        protected JTextField tf_hei;
        protected JButton bn_go;
        protected JButton bn_app;
        protected JButton bn_del;

        public HallBox (HallGraph hg)
        {
            this.hg = hg;
            Hall hall = hg.getHall();

            lb_img = new JLabel(new ImageIcon(
                            PlotCanvas.makeImage(hall, 96, 96, 2, 2)));
            tf_nam = new JTextField(hg.getHall().getName());
            tf_hei = new JTextField(
                    String.format("%.02f", hg.getHall().getHeight()));
            bn_go = new JButton(BnacEditor.gs("GO"));
            bn_app = new JButton(BnacEditor.gs("APPLY"));
            bn_del = new JButton(BnacEditor.gs("DELETE"));

            GridBagConstraints c = new GridBagConstraints();
            setLayout(new GridBagLayout());

            c.gridx = c.gridy = 0;
            c.gridwidth = 2;
            c.gridheight = 3;
            c.fill = GridBagConstraints.BOTH;
            c.insets = new Insets(2, 5, 2, 5);
            c.weighty = 1.0;
            add(lb_img, c);

            c.gridx = GridBagConstraints.RELATIVE;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = GridBagConstraints.HORIZONTAL;
            add(new JLabel(String.format("%s:", BnacEditor.gs("NAME")),
                        JLabel.TRAILING), c);
            add(tf_nam, c);
            c.gridx = 2;
            c.gridy = 1;
            add(new JLabel(String.format("%s:", BnacEditor.gs("HEIGHT")),
                        JLabel.TRAILING), c);
            c.gridx = 3;
            add(tf_hei, c);

            c.gridx = 2;
            c.gridy = 2;
            add(bn_app, c);
            c.gridx = 3;
            add(bn_del, c);

            c.fill = GridBagConstraints.BOTH;
            c.gridx = 4;
            c.gridy = 0;
            c.gridheight = 3;
            c.gridwidth = 2;
            add(bn_go, c);

            setBorder(BorderFactory.createEtchedBorder());

            bn_go.addActionListener(this);
            bn_app.addActionListener(this);
            bn_del.addActionListener(this);
        }

        public void setSelected (boolean sel)
        {
            if (sel) {
                setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
            } else {
                setBorder(BorderFactory.createEtchedBorder());
            }
        }

        public void actionPerformed (ActionEvent ae)
        {
            if (ae.getSource() == bn_go) {
                project.setCurrentHall(hg.getId());
            } else if (ae.getSource() == bn_app) {
                project.pushCommand(new EditHallCommand(
                            project, hg, tf_nam.getText(),
                            Float.parseFloat(tf_hei.getText())));
            } else if (ae.getSource() == bn_del) {
                project.pushCommand(new RemoveHallGraphCommand(
                            project, hg.getId()));
            }
        }

        public void update ()
        {
            lb_img.setIcon(new ImageIcon(
                        PlotCanvas.makeImage(hg.getHall(), 96, 96, 2, 2)));
            tf_nam.setText(hg.getHall().getName());
            tf_hei.setText(String.format("%.02f", hg.getHall().getHeight()));
        }

        public String getHallId () { return hg.getId(); }

        public HallGraph getHallGraph () { return hg; }
    }
}

