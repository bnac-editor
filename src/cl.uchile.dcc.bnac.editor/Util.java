/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import java.awt.Graphics;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.vecmath.Color3f;
import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.TexCoord2f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Geometry;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedQuadArray;
import javax.media.j3d.IndexedTriangleStripArray;
import javax.media.j3d.Material;
import javax.media.j3d.QuadArray;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.swing.JFrame;
import javax.swing.JComponent;

public class Util
{
    final static protected float LN_2 = 0.693147181f;

    static public Geometry rectMesh (float w, float h, float x, float y,
            float z, int divs)
    {
        return rectMesh(w, h, x, y, z, divs, false, 0.0f, 0.0f);
    }

    static public Geometry rectMesh (float w, float h, float x, float y,
            float z, int divs, boolean wTex, float texw, float texh)
    {
        IndexedQuadArray iqa;
        Vector3f nrm = new Vector3f(0.0f, 0.0f, 1.0f);
        int format = GeometryArray.COORDINATES | GeometryArray.NORMALS;
        Point3f[] coords = Util.rectMeshCoords(w, h, x, y, z, divs);
        int[] inds = Util.rectMeshInds(divs);

        if (wTex) { format |= GeometryArray.TEXTURE_COORDINATE_2; }

        iqa = new IndexedQuadArray(coords.length, format, inds.length);
        iqa.setCoordinates(0, coords);
        iqa.setCoordinateIndices(0, inds);
        for (int i=0; i<coords.length; ++i) { iqa.setNormal(i, nrm); }
        for (int i=0; i<inds.length; ++i) { iqa.setNormalIndex(i, 0); }

        if (wTex) {
            TexCoord2f[] texcoords =
                Util.rectMeshTexCoords(texw, texh, divs);
            iqa.setTextureCoordinates(0, 0, texcoords);
            iqa.setTextureCoordinateIndices(0, 0, inds);
        }

        return iqa;
    }

    static public Point3f[] rectMeshCoords (float w, float h, float x,
            float y, float z, int divs)
    {
        Point3f[] verts = {
            new Point3f(x, y, z),
            new Point3f(x+w, y, z),
            new Point3f(x+w, y+h, z),
            new Point3f(x, y+h, z)
        };

        Point3f[] ps = new Point3f[(divs+1)*(divs+1)];
        for (int i=0; i<=divs; ++i) {
            ps[i*(divs+1)] = new Point3f(verts[0]);
            ps[i*(divs+1)].interpolate(verts[3], (i*1.0f)/divs); 
            ps[(i+1)*(divs+1)-1] = new Point3f(verts[1]);
            ps[(i+1)*(divs+1)-1].interpolate(verts[2], (i*1.0f)/divs); 
            for (int j=1; j<=divs-1; ++j) {
                ps[i*(divs+1)+j] = new Point3f(ps[i*(divs+1)]);
                ps[i*(divs+1)+j].interpolate(
                        ps[(i+1)*(divs+1)-1], (j*1.0f)/divs);
            }
        }

        return ps;
    }

    static public TexCoord2f[] rectMeshTexCoords (float w, float h, int divs)
    {
        TexCoord2f[] verts = {
            new TexCoord2f(0.0f, 0.0f),
            new TexCoord2f(w, 0.0f),
            new TexCoord2f(w, h),
            new TexCoord2f(0, h)
        };

        TexCoord2f[] ps = new TexCoord2f[(divs+1)*(divs+1)];
        for (int i=0; i<=divs; ++i) {
            ps[i*(divs+1)] = new TexCoord2f(verts[0]);
            ps[i*(divs+1)].interpolate(verts[3], (i*1.0f)/divs); 
            ps[(i+1)*(divs+1)-1] = new TexCoord2f(verts[1]);
            ps[(i+1)*(divs+1)-1].interpolate(verts[2], (i*1.0f)/divs); 
            for (int j=1; j<=divs-1; ++j) {
                ps[i*(divs+1)+j] = new TexCoord2f(ps[i*(divs+1)]);
                ps[i*(divs+1)+j].interpolate(
                        ps[(i+1)*(divs+1)-1], (j*1.0f)/divs);
            }
        }

        return ps;
    }

    static public int[] rectMeshInds (int divs)
    {
        int[] inds = new int[divs*divs*4];
        for (int i=0; i<divs; ++i) {
            for (int j=0; j<divs; ++j) {
                inds[4*(i*divs+j)] = i*(divs+1)+j;
                inds[4*(i*divs+j)+1] = i*(divs+1)+j+1;
                inds[4*(i*divs+j)+2] = (i+1)*(divs+1)+j+1;
                inds[4*(i*divs+j)+3] = (i+1)*(divs+1)+j;
            }
        }
        return inds;
    }

    static public Geometry polygonMesh (Point2f[] corners, int divs, int segs,
            boolean wTex)
    {
        Vector3f nrm = new Vector3f();
        if (cl.uchile.dcc.bnac.Util.isClockwise(corners)) {
            nrm.z = 1.0f;
        } else {
            nrm.z = -1.0f;
        }

        int format = GeometryArray.COORDINATES | GeometryArray.NORMALS;
        Point3f[] coords = Util.polygonMeshCoords(corners, divs, segs);
        int[] inds = Util.polygonMeshInds(corners.length, divs, segs); 
        int[] indCount = new int[corners.length*segs];
        for (int i=0; i<indCount.length; ++i) { indCount[i] = 1+2*divs; }

        /*
        IndexedQuadArray iqa =
            new IndexedQuadArray(coords.length, format, inds.length);
            */
        IndexedTriangleStripArray iqa = new IndexedTriangleStripArray(
                coords.length, format, inds.length, indCount);
        iqa.setCoordinates(0, coords);
        iqa.setCoordinateIndices(0, inds);
        for (int i=0; i<coords.length; ++i) { iqa.setNormal(i, nrm); }
        for (int i=0; i<inds.length; ++i) { iqa.setNormalIndex(i, 0); }

        return iqa;
    }

    static public Point3f[] polygonMeshCoords (Point2f[] coords, int divs,
            int segs)
    {
        Point3f[] ps = new Point3f[coords.length*divs*segs+1];

        ps[0] = new Point3f();
        for (Point2f p: coords) {
            ps[0].x += p.x;
            ps[0].y += p.y;
        }
        ps[0].scale(1.0f/coords.length);

        coords = Util.refineCorners(coords, segs);

        Point3f tmp = new Point3f();
        for (int i=0; i<divs; ++i) {
            for (int j=0; j<coords.length; ++j) {
                tmp.set(coords[j].x, coords[j].y, 0.0f);
                ps[i*coords.length+j+1] = new Point3f(ps[0]);
                ps[i*coords.length+j+1].interpolate(tmp, (i+1.0f)/divs);
            }
        }

        return ps;
    }

    static public int[] polygonMeshInds (int vcs, int divs, int segs)
    {
        vcs *= segs;
        int[] inds = new int[vcs*(1+2*divs)];
        for (int i=0; i<vcs; ++i) {
            inds[i*(1+2*divs)] = 0;
            for (int j=0; j<divs; ++j) {
                inds[i*(1+2*divs)+2*j+1] = 1+j*vcs+(i+1)%vcs;
                inds[i*(1+2*divs)+2*j+2] = 1+j*vcs+i;
            }
        }
        return inds;
    }

    static public TexCoord2f[] polygonMeshTexCoords (Point2f[] coords,
            int divs, int segs)
    {
        return new TexCoord2f[0];
    }

    static public Point2f[] refineCorners (Point2f[] ps, int segs)
    {
        Point2f[] ret = new Point2f[segs*ps.length];
        int k;
        for (int i=0; i<ps.length; ++i) {
            k = (i+1)%ps.length;

            ret[segs*i] = new Point2f(ps[i]);
            ret[segs*k] = new Point2f(ps[k]);

            for (int j=0; j<segs; ++j) {
                ret[segs*i+j] = new Point2f(ret[segs*i]);
                ret[segs*i+j].interpolate(ret[segs*k], (j*1.0f)/segs);
            }
        }
        return ret;
    }

    static public float angle (Vector2f v)
    {
        boolean xZero = Math.abs(v.x) < 0.01f;
        boolean yZero = Math.abs(v.y) < 0.01f;
        boolean xPos = v.x > 0.00f;
        boolean yPos = v.y > 0.00f;

        if (xZero && yZero) { return 0.0f; }

        if (xZero) {
            if (yPos) {
                return (float) Math.PI/2;
            } else {
                return (float) -Math.PI/2;
            }
        } else if (yZero) {
            if (xPos) {
                return 0.00f;
            } else {
                return (float) Math.PI;
            }
        } else {
            if (xPos) { return (float) Math.atan(v.y/v.x); }
            else { return (float) (Math.atan(v.y/v.x) - Math.PI); }
        }
    }

    static public String gs (String key) { return BnacEditor.gs(key); }

    static public boolean isNearLine (Point2f a, Point2f b, Point2f p,
            float maxd)
    {
        float eps = 0.001f;
        if (Math.abs(a.x-b.x) < eps) {
            /* vertical line */
            if (Math.abs(p.x-a.x) < maxd) {
                if (a.y > b.y) {
                    return (p.y <= a.y && p.y >= b.y);
                } else {
                    return (p.y <= b.y && p.y >= a.y);
                }
            } else { return false; }
        } else if (Math.abs(a.y-b.y) < eps) {
            /* horizontal line */
            if (Math.abs(p.y-a.y) < maxd) {
                if (a.x > b.x) {
                    return (p.x <= a.x && p.x >= b.x);
                } else {
                    return (p.x <= b.x && p.x >= a.x);
                }
            } else { return false; }
        } else {
            float theta = (float) ((a.x<b.x) ?
                    -Math.asin((b.y-a.y)/a.distance(b)) :
                    -Math.asin((a.y-b.y)/a.distance(b)));
            float sin = (float) Math.sin(theta);
            float cos = (float) Math.cos(theta);
            return isNearLine(
                    new Point2f(cos*a.x-sin*a.y, sin*a.x+cos*a.y),
                    new Point2f(cos*b.x-sin*b.y, sin*b.x+cos*b.y),
                    new Point2f(cos*p.x-sin*p.y, sin*p.x+cos*p.y),
                    maxd);
        }
    }

    static public <T extends Tuple2f> T toTuple2f (Tuple3f t, T t2)
    {
        t2.set(t.x, -t.z);
        return t2;
    }

    static protected BranchGroup makeFloor (Point2f[] corners, int divs,
            int segs)
    {
        Geometry geom = Util.polygonMesh(corners, divs, segs, false);

        Appearance ap = new Appearance();
        Material mat = new Material(
                new Color3f(0.22f, 0.17f, 0.11f), // ambient
                new Color3f(0.0f, 0.0f, 0.0f), // emissive
                new Color3f(0.43f, 0.33f, 0.21f), // diffuse
                new Color3f(1.0f, 1.0f, 1.0f), // specular
                100.0f);
        ap.setMaterial(mat);

        BranchGroup floorGraph = new BranchGroup();
        floorGraph.setCapability(BranchGroup.ALLOW_DETACH);
        TransformGroup tg = new TransformGroup();
        Transform3D t3d = new Transform3D();
        t3d.rotX((float) -Math.PI/2);
        tg.setTransform(t3d);
        tg.addChild(new Shape3D(geom, ap));
        floorGraph.addChild(tg);
        floorGraph.setPickable(false);
        return floorGraph;
    }

    static protected BranchGroup makeCeiling (Point2f[] corners, float height,
            int divs, int segs)
    {
        Point2f[] c2 = new Point2f[corners.length];
        for (int i=0; i<corners.length; ++i) {
            c2[c2.length-1-i] = corners[i];
        }
        Geometry geom = Util.polygonMesh(c2, divs, segs, false);

        Appearance ap = new Appearance();
        Material mat = new Material(
                new Color3f(0.2f, 0.2f, 0.2f), // ambient
                new Color3f(0.0f, 0.0f, 0.0f), // emissive
                new Color3f(0.9f, 0.9f, 0.9f), // diffuse
                new Color3f(0.0f, 0.0f, 0.0f), // specular
                0.0f);
        ap.setMaterial(mat);

        BranchGroup ceilGraph = new BranchGroup();
        ceilGraph.setCapability(BranchGroup.ALLOW_DETACH);
        TransformGroup tg = new TransformGroup();
        Transform3D t3d = new Transform3D();
        t3d.rotX((float) -Math.PI/2);
        t3d.setTranslation(new Vector3f(0.0f, height, 0.0f));
        tg.setTransform(t3d);
        tg.addChild(new Shape3D(geom, ap));
        ceilGraph.addChild(tg);
        ceilGraph.setPickable(false);
        return ceilGraph;
    }

    static protected String sanitize (String s)
    {
        char[] chs = s.toCharArray();
        char c;
        String allowed =
            "0123456789" +
            "abcdefghijklmnopqrstuvwxyz" +
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "-_";

        for (int i=0; i<chs.length; ++i) {
            if (allowed.indexOf(chs[i]) < 0) { chs[i] = '_'; }
        }

        return new String(chs);
    }

    static public Vector2f normal (Vector2f v)
    {
        Vector2f ret = new Vector2f(-v.y, v.x);
        ret.normalize();
        return ret;
    }

    static public BufferedImage padToPowerOf2 (BufferedImage img)
    {
        int new_w;
        int new_h;
        int w = img.getWidth();
        int h = img.getHeight();
        double log2_w = Math.log(w) / LN_2;
        double log2_h = Math.log(h) / LN_2;

        BnacEditor.trace("padding from %dx%d", w, h);

        BnacEditor.trace("logW = %f, logH = %f", log2_w, log2_h);

        if ((log2_w - (int) log2_w) > 0.00001) {
            new_w = (int) Math.pow(2.0, (int) (log2_w + 1.0));
        } else {
            new_w = w;
        }

        if ((log2_h - (int) log2_h) > 0.00001) {
            new_h = (int) Math.pow(2.0, (int) (log2_h + 1.0));
        } else {
            new_h = h;
        }

        BnacEditor.trace("padding to %dx%d", new_w, new_h);
        BufferedImage ret = new BufferedImage(new_w, new_h,
                BufferedImage.TYPE_INT_RGB);
        Graphics g = ret.getGraphics();
        g.drawImage(img, 0, new_h-h, null);
        return ret;
    }

    static public void showFrame (String title, JComponent comp)
    {
        JFrame jf = new JFrame(title);
        jf.add(comp);
        jf.pack();
        jf.setVisible(true);
    }

    static public Image loadImage (File file, int maxlen) throws IOException
    {
        BufferedImage buf = ImageIO.read(file);
        Image img = buf;

        if (buf.getWidth() > maxlen || buf.getHeight() > maxlen) {
            if (buf.getWidth() > buf.getHeight()) {
                img = buf.getScaledInstance(maxlen, -1,
                        BufferedImage.SCALE_FAST);
            } else {
                img = buf.getScaledInstance(-1, maxlen,
                        BufferedImage.SCALE_FAST);
            }
        }

        return img;
    }

    private Util () { }
}

