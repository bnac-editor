/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.CObject;
import cl.uchile.dcc.bnac.Capture;
import cl.uchile.dcc.bnac.MuseumEnvironment;
import cl.uchile.dcc.bnac.Route;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class TranslateTool extends JPanel
    implements ActionListener, ItemListener
{
    protected Project project;

    protected JPanel pnMain;
    protected JComboBox cbType;
    protected JButton bnClose;

    public TranslateTool (Project project)
    {
        super(new BorderLayout());
        this.project = project;

        pnMain = new JPanel();
        cbType = new JComboBox();
        bnClose = new JButton(BnacEditor.gs("CLOSE"));

        buildUi();

        cbType.setModel(new DefaultComboBoxModel(Type.values()));

        pnMain.add(new CaptureTranslateBox());

        cbType.addItemListener(this);
        bnClose.addActionListener(this);
    }

    protected void buildUi ()
    {
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnMain.setBorder(BorderFactory.createEtchedBorder());

        JPanel caca = new JPanel(new FlowLayout(FlowLayout.LEADING, 2, 2));
        caca.add(new JLabel(String.format("%s:",
                        BnacEditor.gs("TYPE_TO_TRANSLATE"))));
        caca.add(cbType);
        add(caca, BorderLayout.NORTH);
        add(pnMain, BorderLayout.CENTER);
        add(bnClose, BorderLayout.SOUTH);
    }

    public void actionPerformed (ActionEvent e)
    {
        if (e.getSource() == bnClose) {
            SwingUtilities.getWindowAncestor(this).setVisible(false);
        }
    }

    public void itemStateChanged (ItemEvent e)
    {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            pnMain.removeAll();
            Type t = (Type) e.getItem();
            switch (t) {
                case Capture:
                    pnMain.add(new CaptureTranslateBox());
                    break;
                case Route:
                    pnMain.add(new RouteTranslateBox());
                    break;
            }
            SwingUtilities.getWindowAncestor(this).pack();
        }
    }

    class CaptureTranslateBox extends JPanel
        implements ActionListener, ItemListener
    {
        protected Language lang;

        protected JComboBox cbCaps;
        protected JComboBox cbLangs;
        protected JTextField tfAuthor;
        protected JTextField tfTitle;
        protected JTextField tfType;
        protected JButton bnSave;
        protected JButton bnOrig;

        public CaptureTranslateBox ()
        {
            super(new GridBagLayout());

            lang = Language.values()[0];

            cbCaps = new JComboBox();
            cbLangs = new JComboBox();
            tfAuthor = new JTextField();
            tfTitle = new JTextField();
            tfType = new JTextField();
            bnSave = new JButton(BnacEditor.gs("SAVE"));
            bnOrig = new JButton(BnacEditor.gs("ORIGINAL_VALUES"));

            buildUi();

            String[] caps =
                new String[project.getMuseumEnvironment().captureNo()];
            int i=0;
            for (Capture ca: project.getMuseumEnvironment().captureArray()) {
                caps[i++] = String.format("%s - %s",
                        ca.getReferencedObject().get("Author"),
                        ca.getReferencedObject().get("Title"));
            }
            cbCaps.setModel(new DefaultComboBoxModel(caps));

            cbLangs.setModel(new DefaultComboBoxModel(Language.values()));

            cbCaps.addItemListener(this);
            cbLangs.addItemListener(this);
            bnOrig.addActionListener(this);
            bnSave.addActionListener(this);

            fillCaptureInfo();
        }

        protected void buildUi ()
        {
            pnMain.removeAll();

            pnMain.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.ipadx = 5;
            c.ipady = 5;
            c.fill = GridBagConstraints.HORIZONTAL;

            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 2;
            add(cbCaps, c);
            c.gridx = GridBagConstraints.RELATIVE;
            add(cbLangs, c);

            c.anchor = GridBagConstraints.WEST;
            c.gridwidth = 1;
            c.gridx = 0;
            c.gridy = 1;
            add(new JLabel(BnacEditor.gs("AUTHOR")), c);
            c.gridy = GridBagConstraints.RELATIVE;
            add(new JLabel(BnacEditor.gs("TITLE")), c);
            add(new JLabel(BnacEditor.gs("TYPE")), c);

            c.gridwidth = 3;
            c.gridx = 1;
            c.gridy = 1;
            add(tfAuthor, c);
            c.gridy = GridBagConstraints.RELATIVE;
            add(tfTitle, c);
            add(tfType, c);

            c.gridwidth = 1;
            c.gridx = 2;
            c.gridy = 4;
            add(bnOrig, c);

            c.gridwidth = 1;
            c.gridx = 3;
            c.gridy = 4;
            add(bnSave, c);

            setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        }

        public void actionPerformed (ActionEvent e)
        {
            Capture c = project.getMuseumEnvironment().
                captureArray()[cbCaps.getSelectedIndex()];
            CObject obj = c.getReferencedObject();
            if (e.getSource() == bnOrig) {
                tfAuthor.setText(obj.get("Author"));
                tfTitle.setText(obj.get("Title"));
                tfType.setText(obj.get("Type"));
                bnSave.doClick();
            } else if (e.getSource() == bnSave) {
                project.getMuseumEnvironment().addString(
                        obj.getId(), lang.getCode(),
                        "Author", tfAuthor.getText());
                project.getMuseumEnvironment().addString(
                        obj.getId(), lang.getCode(),
                        "Title", tfTitle.getText());
                project.getMuseumEnvironment().addString(
                        obj.getId(), lang.getCode(),
                        "Type", tfType.getText());
            }
        }

        public void itemStateChanged (ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (e.getSource() == cbCaps) {
                    fillCaptureInfo();
                } else if (e.getSource() == cbLangs) {
                    lang = (Language) e.getItem();
                    fillCaptureInfo();
                }
            }
        }

        public void fillCaptureInfo ()
        {
            Capture c = project.getMuseumEnvironment().
                captureArray()[cbCaps.getSelectedIndex()];
            CObject obj = c.getReferencedObject();
            String id = obj.getId();
            String l = lang.getCode();
            MuseumEnvironment me = project.getMuseumEnvironment();
            if (me.getString(id, l, "Author") != null) {
                tfAuthor.setText(me.getString(id, l, "Author"));
            } else {
                tfAuthor.setText("");
            }
            if (me.getString(id, l, "Title") != null) {
                tfTitle.setText(me.getString(id, l, "Title"));
            } else {
                tfTitle.setText("");
            }
            if (me.getString(id, l, "Type") != null) {
                tfType.setText(me.getString(id, l, "Type"));
            } else {
                tfType.setText("");
            }
        }
    }

    class RouteTranslateBox extends JPanel
        implements ActionListener, ItemListener
    {
        protected Language lang;
        protected JComboBox cbRoutes;
        protected JComboBox cbLangs;
        protected JTextField tfName;
        protected JButton bnSave;

        public RouteTranslateBox ()
        {
            super(new GridBagLayout());

            lang = Language.values()[0];
            cbLangs = new JComboBox();
            cbRoutes = new JComboBox();
            tfName = new JTextField(20);
            bnSave = new JButton(BnacEditor.gs("SAVE"));

            buildUi();

            String[] rts = new
                String[project.getMuseumEnvironment().routeArray().length];
            int i=0;
            for (Route r: project.getMuseumEnvironment().routeArray()) {
                rts[i++] = r.getId();
            }
            cbRoutes.setModel(new DefaultComboBoxModel(rts));
            cbLangs.setModel(new DefaultComboBoxModel(Language.values()));

            cbRoutes.addItemListener(this);
            cbLangs.addItemListener(this);
            bnSave.addActionListener(this);

            fillRouteInfo();
        }

        protected void buildUi ()
        {
            GridBagConstraints c = new GridBagConstraints();

            c.ipadx = 5;
            c.ipady = 5;
            c.fill = GridBagConstraints.HORIZONTAL;

            c.gridwidth = 2;
            c.gridx = 0;
            c.gridy = 0;
            add(cbRoutes, c);

            c.gridwidth = 2;
            c.gridx = 2;
            c.gridy = 0;
            add(cbLangs, c);

            c.gridwidth = 1;
            c.gridx = 0;
            c.gridy = 1;
            add(new JLabel(BnacEditor.gs("NAME")), c);
            c.gridwidth = 3;
            c.gridx = 1;
            add(tfName, c);
            
            c.gridwidth = 1;
            c.gridx = 3;
            c.gridy = 2;
            add(bnSave, c);

            setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        }

        public void actionPerformed (ActionEvent e)
        {
            Route route = project.getMuseumEnvironment().routeArray()[
                cbRoutes.getSelectedIndex()];
            if (e.getSource() == bnSave) {
                project.getMuseumEnvironment().addString(
                        route.getId(), lang.getCode(),
                        "Name", tfName.getText());
            }
        }

        public void itemStateChanged (ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (e.getSource() == cbRoutes) {
                    fillRouteInfo();
                } else if (e.getSource() == cbLangs) {
                    lang = (Language) e.getItem();
                    fillRouteInfo();
                }
            }
        }

        public void fillRouteInfo ()
        {
            Route route = project.getMuseumEnvironment().routeArray()[
                cbRoutes.getSelectedIndex()];
            String s = project.getMuseumEnvironment().getString(
                    route.getId(), lang.getCode(), "Name");
            if (s != null) {
                tfName.setText(s);
            } else {
                tfName.setText("");
            }
        }
    }

    public enum Type
    {
        Capture,
        Route;
    }
}

