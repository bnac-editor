/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import java.awt.Insets;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CreateProjectDialogue extends ProjectDialogue
    implements ActionListener
{
    protected JTextField tf_ttl;
    protected JTextField tf_dir;
    protected JButton bn_brw;
    protected JButton bn_ccl;
    protected JButton bn_ok;

    public CreateProjectDialogue ()
    {
        super(null, BnacEditor.gs("CREATE_PROJECT"));

        tf_ttl = new JTextField(15);
        tf_dir = new JTextField(10);
        bn_brw = new JButton(BnacEditor.gs("BROWSE"));
        bn_ccl = new JButton(BnacEditor.gs("CANCEL"));
        bn_ok = new JButton(BnacEditor.gs("OK"));


        bn_brw.addActionListener(this);
        bn_ok.addActionListener(this);
        bn_ccl.addActionListener(this);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(2, 5, 2, 5);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.weighty = 1.0;
        c.gridx = c.gridy = 0;
        add(new JLabel(BnacEditor.gs("PROJECT_TITLE")), c);

        c.weightx = 1.0;
        c.gridx = GridBagConstraints.RELATIVE;
        c.gridwidth = 3;
        add(tf_ttl, c);

        c.gridy = 1;
        c.weightx = 0.0;
        c.gridwidth = 1;
        add(new JLabel(BnacEditor.gs("PROJECT_PATH")), c);

        c.weightx = 0.9;
        c.gridwidth = 2;
        add(tf_dir, c);

        c.weightx = 0.1;
        c.gridwidth = 1;
        add(bn_brw, c);

        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 1.0;
        c.gridwidth = 2;
        add(bn_ccl, c);
        c.gridx = 2;
        add(bn_ok, c);
    }

    public void actionPerformed (ActionEvent e)
    {
        if (e.getSource() == bn_brw) {
            JFileChooser jfc =
                new JFileChooser(BnacEditor.config.getProperty("user.dir"));
            jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                tf_dir.setText(jfc.getSelectedFile().getAbsolutePath());
            }
        } else if (e.getSource () == bn_ccl) {
            jf.setVisible(false);
        } else if (e.getSource() == bn_ok) {
            File root = new File(tf_dir.getText());
            
            if (root.isDirectory()) {
                File xml;
                File dir;
                
                xml = new File(root, "project.xml");
                try {
                    xml.createNewFile();
                } catch (IOException ioe) {
                    BnacEditor.error(String.format(
                                "unable to create file project.xml: %s",
                                ioe.getMessage()));
                } 
                xml = new File(root, "info.xml");
                try {
                    xml.createNewFile();
                } catch (IOException ioe) {
                    BnacEditor.error(String.format(
                                "unable to create file info.xml: %s",
                                ioe.getMessage()));
                }

                
                dir = new File(root, "img");
                if (!dir.isDirectory() && !dir.mkdir()) {
                    BnacEditor.error("unable to create directory img");
                    return;
                }
                dir = new File(root, "extra");
                if (!dir.isDirectory() && !dir.mkdir()) {
                    BnacEditor.error("unable to create directory extra");
                    return;
                }

                BnacEditor.addProject(
                        Project.createProject(root, tf_ttl.getText()));
            } else {
                BnacEditor.error(
                        "cannot make project in %d: not a directory");
            }
            jf.setVisible(false);
        }
    }
}


