/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Door;
import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.Link;
import cl.uchile.dcc.bnac.Wall;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Vector2f;

public class NavigationView extends ProjectView
    implements ActionListener
{
    protected ArrayList<LinkBox> linkList;

    protected Box box_list;

    public NavigationView (Project project)
    {
        super(project);
        linkList = new ArrayList<LinkBox>();
        box_list = new Box(BoxLayout.PAGE_AXIS);

        LinkBox lb;
        for (Link link: project.getMuseumEnvironment().linkArray()) {
            lb = new LinkBox(link);
            linkList.add(lb);
            box_list.add(lb);
        }

        JButton bn = new JButton(BnacEditor.gs("CREATE_LINK"));
        bn.addActionListener(this);

        setLayout(new BorderLayout(0, 2));
        add(bn, BorderLayout.NORTH);
        add(new JScrollPane(box_list,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                BorderLayout.CENTER);
    }

    public void addLink (Link l)
    {
        LinkBox lb = new LinkBox(l);
        linkList.add(lb);
        box_list.add(lb);
        ((JScrollPane) getComponent(1)).updateUI();
        if (linkList.size() == 1) { project.getNavigationFrame().pack(); }
    }

    public void removeLink (String id)
    {
        LinkBox lb = null;
        for (LinkBox lbb: linkList) {
            if (lbb.getLinkId().equals(id)) {
                lb = lbb;
                box_list.remove(lb);
                linkList.remove(lb);
                ((JScrollPane) getComponent(1)).updateUI();
                return;
            }
        }
    }

    public HallGraph[] linkHalls (String id)
    {
        for (LinkBox lb: linkList) {
            if (lb.getLinkId().equals(id)) {
                HallGraph[] ret = new HallGraph[2];
                ret[0] =
                    project.getHallView().getHallGraph(lb.hallGraph1Id());
                ret[1] =
                    project.getHallView().getHallGraph(lb.hallGraph2Id());
                return ret;
            }
        }
        return null;
    }

    public void actionPerformed (ActionEvent e)
    {
        ProjectDialogue.fireDialogue(new CreateLinkDialogue(project));
    }

    public class LinkBox extends JPanel
            implements ActionListener
    {
        protected Link l;
        protected JLabel lb_wid;
        protected JLabel lb_hei;
        protected JLabel lb_hl1;
        protected JLabel lb_hl2;
        protected JButton bn_edt;
        protected JButton bn_del;

        protected String hl1;
        protected String hl2;

        public LinkBox (Link l)
        {
            this.l = l;

            for (Hall h: project.getMuseumEnvironment().hallArray()) {
                if (hl1 == null && h.getDoor(l.d1) != null) {
                    hl1 = h.getId();
                } else if (hl2 == null && h.getDoor(l.d2) != null) {
                    hl2 = h.getId();
                }

                if (hl1 != null && hl2 != null) { break; }
            }

            Hall h1 = project.getMuseumEnvironment().getHall(hl1);
            Hall h2 = project.getMuseumEnvironment().getHall(hl2);

            Point2f d = h1.getDoor(l.d1).getDimensions();

            lb_wid = new JLabel(String.format("%.02f", d.x), JLabel.CENTER);
            lb_hei = new JLabel(String.format("%.02f", d.y), JLabel.CENTER);
            lb_hl1 = new JLabel(new ImageIcon(new DoorPlotCanvas(
                            h1, h1.getDoor(l.d1), 96, 96, 2, 2).makeImage()));
            lb_hl2 = new JLabel(new ImageIcon(new DoorPlotCanvas(
                            h2, h2.getDoor(l.d2), 96, 96, 2, 2).makeImage()));
            bn_edt = new JButton(BnacEditor.gs("EDIT"));
            bn_del = new JButton(BnacEditor.gs("DELETE"));

            bn_edt.addActionListener(this);
            bn_del.addActionListener(this);

            buildForm();
        }

        public String hallGraph1Id () { return  hl1; }
        public String hallGraph2Id () { return  hl2; }

        protected void buildForm ()
        {
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.insets = new Insets(2, 5, 2, 5);
            c.gridx = c.gridy = 0;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.gridwidth = 3;
            c.weighty = 1.0;
            add(new JLabel(String.format("%s:", BnacEditor.gs("DIMENSIONS")),
                        JLabel.TRAILING), c);

            c.weightx = 1.0;
            c.gridx = GridBagConstraints.RELATIVE;
            c.gridwidth = 1;
            add(lb_wid, c);
            c.weightx = 0.0;
            add(new JLabel("x", JLabel.CENTER), c);
            c.weightx = 1.0;
            add(lb_hei, c);

            c.gridx = 0;
            c.gridy = 1;
            c.gridwidth = 3;
            c.gridheight = 2;
            c.weightx = c.weighty = 1.0;
            c.fill = GridBagConstraints.BOTH;
            add(lb_hl1, c);
            c.gridx = 3;
            add(lb_hl2, c);

            c.gridx = 6;
            c.gridy = 1;
            c.gridwidth = c.gridheight = 1;
            add(bn_edt, c);
            c.gridy = 2;
            add(bn_del, c);

            setBorder(BorderFactory.createEtchedBorder());
        }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bn_del) {
                HallGraph hg1 = project.getHallView().getHallGraph(hl1);
                int widx1 = hg1.getHall().getParentWall(l.d1);
                DoorGraph dg1 = hg1.getDoorGraph(l.d1);

                HallGraph hg2 = project.getHallView().getHallGraph(hl2);
                int widx2 = hg2.getHall().getParentWall(l.d2);
                DoorGraph dg2 = hg2.getDoorGraph(l.d2);

                project.pushCommand(new RemoveLinkCommand(project, l,
                            hg1, widx1, dg1, hg2, widx2, dg2));
            }
        }

        public String getLinkId () { return l.getId(); }

        public class DoorPlotCanvas extends PlotCanvas
        {
            protected Door door;
            protected Color hlColor;
            protected Wall wall;

            public DoorPlotCanvas (Hall hall, Door door,
                    int mx, int my, int bx, int by)
            {
                super(hall, mx, my, bx, by);
                this.door = door;

                hlColor = Color.RED;

                wall = hall.getWall(hall.getParentWall(door.getId()));
            }

            public void paint (Graphics g)
            {
                super.paint(g);

                float width = door.getDimensions().x;
                float offset = door.getPosition().x;

                Vector2f v = new Vector2f();
                Point2f tmp = new Point2f();
                v.sub(wall.end2f(),
                        wall.origin2f());
                float d = v.length();

                tmp.scaleAdd(offset/d, v, wall.origin2f());
                Point2i l = coordsToPix(tmp.x, tmp.y);
                tmp.scaleAdd((offset+width)/d, v,
                        wall.origin2f());
                Point2i r = coordsToPix(tmp.x, tmp.y);

                Graphics2D g2 = (Graphics2D) g;
                Stroke os = g2.getStroke();
                g2.setColor(hlColor);
                g2.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE,
                            BasicStroke.JOIN_BEVEL));
                g2.drawLine(l.x, l.y, r.x, r.y);
                g2.setStroke(os);
                g2.setColor(fgColor);
            }
        }

        public class RemoveLinkCommand extends ProjectCommand
        {
            protected Link link;

            protected HallGraph hg1;
            protected int widx1;
            protected DoorGraph dg1;

            protected HallGraph hg2;
            protected int widx2;
            protected DoorGraph dg2;

            public RemoveLinkCommand (Project project, Link link,
                    HallGraph hg1, int widx1, DoorGraph dg1,
                    HallGraph hg2, int widx2, DoorGraph dg2)
            {
                super(project, "REMOVE_LINK");
                this.link = link;
                
                this.hg1 = hg1;
                this.widx1 = widx1;
                this.dg1 = dg1;

                this.hg2 = hg2;
                this.widx2 = widx2;
                this.dg2 = dg2;
            }

            public void execute ()
            {
                project.getMuseumEnvironment().removeLink(link.getId());
                project.getNavigationView().removeLink(link.getId());
                hg1.removeDoorGraph(dg1.getId());
                hg2.removeDoorGraph(dg2.getId());
            }

            public void undo ()
            {
                hg1.addDoorGraph(widx1, dg1);
                hg2.addDoorGraph(widx2, dg2);
                project.getMuseumEnvironment().addLink(link);
                project.getNavigationView().addLink(link);
            }
        }
    }
}

