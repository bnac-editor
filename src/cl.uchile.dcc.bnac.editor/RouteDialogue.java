/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Capture;
import cl.uchile.dcc.bnac.Route;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class RouteDialogue extends ProjectDialogue
    implements ActionListener
{
    protected Route route;

    protected ArrayList<CaptureBox> capList;
    protected Box box_list;
    protected JScrollPane sp_main;
    protected NewCaptureBox ncb;

    public RouteDialogue (Project project, Route r)
    {
        super(project, r.getId());
        route = r;

        capList = new ArrayList<CaptureBox>();
        box_list = new Box(BoxLayout.PAGE_AXIS);
        sp_main = new JScrollPane(box_list,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        ncb = null;

        buildUi();

        BnacEditor.trace("--[route \"%s\"]--------", r.getId());
        CaptureBox cb;
        Capture c;
        int i = 0;
        for (String s: route.captureArray()) {
            c = project.getMuseumEnvironment().getCapture(s);
            if (c != null) {
                cb = new CaptureBox(c, ++i);
                BnacEditor.trace("%d. %s", i, cb.getCaptureId());
                capList.add(cb);
                box_list.add(cb);
                c = null;
            }
        }
        BnacEditor.trace("--------------------", r.getId());
    }

    protected void buildUi ()
    {
        setLayout(new BorderLayout(0, 2));

        JButton nc = new JButton(BnacEditor.gs("ADD_CAPTURE"));
        nc.addActionListener(this);
        
        add(nc, BorderLayout.NORTH);
        add(new JScrollPane(box_list,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                BorderLayout.CENTER);
    }

    public void actionPerformed (ActionEvent e)
    {
        if (route.captureNo() > project.getMuseumEnvironment().captureNo()) {
            return;
        }
        box_list.add((ncb = new NewCaptureBox()));
        sp_main.getVerticalScrollBar().setValue(
                sp_main.getVerticalScrollBar().getMaximum());
        box_list.setVisible(false);
        box_list.setVisible(true);
        sp_main.setVisible(false);
        sp_main.setVisible(true);
    }

    protected void removeNewCaptureBox ()
    {
        if (ncb != null) { box_list.remove(ncb); }
        ncb = null;
        box_list.setVisible(false);
        box_list.setVisible(true);
    }

    public void addCapture (Capture c)
    {
        CaptureBox cb = new CaptureBox(c, capList.size()+1);
        capList.add(cb);
        box_list.add(cb);
        box_list.setVisible(false);
        box_list.setVisible(true);
    }

    public void removeCapture (String id)
    {
        for (int i=0; i<capList.size(); ++i) {
            if (capList.get(i).getCaptureId().equals(id)) {
                capList.remove(i);
                box_list.remove(i);
                break;
            }
        }

        for (int i=0; i<capList.size(); ++i) {
            capList.get(i).setIndex(i+1);
        }

        box_list.setVisible(false);
        box_list.setVisible(true);
    }

    public void moveCapture (String id, int idx)
    {
        for (int i=0; i<capList.size(); ++i) {
            if (capList.get(i).getCaptureId().equals(id)) {
                CaptureBox cb = capList.remove(i);
                box_list.remove(i);
                if (idx < capList.size()) {
                    capList.add(idx, cb);
                    box_list.add(cb, idx);
                } else {
                    capList.add(cb);
                    box_list.add(cb);
                }
                break;
            }
        }

        for (int i=0; i<capList.size(); ++i) {
            capList.get(i).setIndex(i+1);
        }
    }

    public void moveBackCapture (String id)
    {
        for (int i=0; i<capList.size(); ++i) {
            if (capList.get(i).getCaptureId().equals(id)) {
                if (i > 0) {
                    project.pushCommand(
                            new MoveCaptureCommand(project, id, i, i-1));
                }
                return;
            }
        }
    }

    public void moveForwardCapture (String id)
    {
        for (int i=0; i<capList.size(); ++i) {
            if (capList.get(i).getCaptureId().equals(id)) {
                if (i < capList.size()) {
                    project.pushCommand(
                            new MoveCaptureCommand(project, id, i, i+1));
                }
                return;
            }
        }
    }

    class CaptureBox extends JPanel
        implements ActionListener
    {
        protected Capture cap;

        protected JLabel lbl_ttl;
        protected JButton bn_up;
        protected JButton bn_dn;
        protected JButton bn_del;

        public CaptureBox (Capture cap, int idx)
        {
            super(new GridBagLayout());
            this.cap = cap;

            lbl_ttl = new JLabel(String.format("%d. %s - %s", idx,
                        cap.getReferencedObject().get("Author"),
                        cap.getReferencedObject().get("Title")),
                    JLabel.LEADING);
            bn_up = new JButton(BnacEditor.gs("UP"));
            bn_dn = new JButton(BnacEditor.gs("DOWN"));
            bn_del = new JButton(BnacEditor.gs("DELETE"));

            buildUi();

            bn_up.addActionListener(this);
            bn_dn.addActionListener(this);
            bn_del.addActionListener(this);
        }

        public void buildUi ()
        {
            GridBagConstraints c = new GridBagConstraints();

            c.anchor = GridBagConstraints.LINE_START;
            c.gridx = c.gridy = 0;
            c.gridwidth = 3;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(2, 5, 2, 5);
            c.weightx = 1.0;
            c.weighty = 1.0;
            c.ipady = 3;
            c.ipadx = 3;
            add(lbl_ttl, c);

            c.weightx = 0.0;
            c.ipady = c.ipadx = 0;
            c.gridwidth = 1;
            c.gridx = GridBagConstraints.RELATIVE;
            add(bn_up, c);

            add(bn_dn, c);

            add(bn_del, c);

            setBorder(BorderFactory.createEtchedBorder());

            java.awt.Dimension d = getPreferredSize();
            d.width = Integer.MAX_VALUE;
            setMaximumSize(d);
        }

        public void setIndex (int idx)
        {
            lbl_ttl.setText(String.format("%d. %s - %s", idx,
                        cap.getReferencedObject().get("Author"),
                        cap.getReferencedObject().get("Title")));
        }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bn_up) {
                moveBackCapture(cap.getId());
            } else if (e.getSource() == bn_dn) {
                moveForwardCapture(cap.getId());
            } else if (e.getSource() == bn_del) {
                project.pushCommand(new RemoveCaptureCommand(project, cap));
            }
        }

        public String getCaptureId () { return cap.getId(); }
    }

    class NewCaptureBox extends JPanel implements ItemListener
    {
        JComboBox cb_caps;
        ArrayList<String> ids;

        public NewCaptureBox ()
        {
            super(new BorderLayout());

            cb_caps = new JComboBox();
            ids = new ArrayList<String>();

            buildUi();

            cb_caps.addItemListener(this);

            cb_caps.addItem(BnacEditor.gs("CHOOSE_CAPTURE"));
            boolean skip;
            for (Capture c: project.getMuseumEnvironment().captureArray()) {
                skip = false;
                for (CaptureBox cb: capList) {
                    if (cb.getCaptureId().equals(c.getId())) {
                        skip = true;
                        break;
                    }
                }
                if (skip) { continue; }
                cb_caps.addItem(String.format("%s - %s",
                            c.getReferencedObject().get("Author"),
                            c.getReferencedObject().get("Title")));
                ids.add(c.getId());
            }
        }

        public void buildUi ()
        {
            add(cb_caps, BorderLayout.CENTER);
        }

        public void itemStateChanged (ItemEvent e)
        {
            BnacEditor.trace("%s: %s",
                    (e.getStateChange()==ItemEvent.SELECTED) ?
                    "selected": "deselected",
                    e.getItem().toString());
            if (cb_caps.getSelectedIndex() > 0 &&
                    e.getStateChange()==ItemEvent.SELECTED) {
                String capid = ids.get(cb_caps.getSelectedIndex()-1);
                BnacEditor.trace("adding: %s", capid);
                project.pushCommand(new AddCaptureCommand(
                            project,
                            project.getMuseumEnvironment().
                            getCapture(capid)));
                removeNewCaptureBox();
            }
        }
    }

    class AddCaptureCommand extends ProjectCommand
    {
        protected Capture capture;

        public AddCaptureCommand (Project project, Capture capture)
        {
            super(project, "ADD_CAPTURE");
            this.capture = capture;
        }

        public void execute ()
        {
            BnacEditor.trace("do AddCaptureCommand");
            route.addCapture(capture.getId());
            addCapture(capture);
        }

        public void undo ()
        {
            BnacEditor.trace("undo AddCaptureCommand");
            route.removeCapture(capture.getId());
            removeCapture(capture.getId());
        }
    }

    class RemoveCaptureCommand extends ProjectCommand
    {
        protected Capture capture;

        public RemoveCaptureCommand (Project project, Capture capture)
        {
            super(project, "REMOVE_CAPTURE");
            this.capture = capture;
        }

        public void execute ()
        {
            BnacEditor.trace("do RemoveCaptureCommand");
            route.removeCapture(capture.getId());
            removeCapture(capture.getId());
        }

        public void undo ()
        {
            BnacEditor.trace("undo RemoveCaptureCommand");
            route.addCapture(capture.getId());
            addCapture(capture);
        }
    }

    class MoveCaptureCommand extends ProjectCommand
    {
        protected String id;
        protected int oldidx;
        protected int newidx;

        public MoveCaptureCommand (Project project, String id,
                int oldidx, int newidx)
        {
            super(project, "MOVE");
            this.id = id;
            this.oldidx = oldidx;
            this.newidx = newidx;
        }

        public void execute ()
        {
            BnacEditor.trace("do MoveCaptureCommand");
            moveCapture(id, newidx);
            route.moveCapture(newidx, id);
        }

        public void undo ()
        {
            BnacEditor.trace("undo MoveCaptureCommand");
            moveCapture(id, oldidx);
            route.moveCapture(oldidx, id);
        }
    }
}

