/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import javax.swing.JFrame;

public class ProjectDialogue extends ProjectView
{
    protected JFrame jf;
    protected String title;

    public ProjectDialogue (Project p, String title)
    {
        super(p);
        this.title = title;
    }

    static public void fireDialogue (ProjectDialogue pd)
    {
        JFrame diag = new JFrame();
        pd.setJFrame(diag);
        diag.add(pd);
        diag.pack();
        diag.setVisible(true);
    }

    protected void setJFrame (JFrame jf)
    {
        this.jf = jf;
        this.jf.setTitle(title);
    }
}

