/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.InfoSet;
import cl.uchile.dcc.bnac.CObject;
import cl.uchile.dcc.bnac.Capture;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

public class InfoTool extends JPanel
    implements ActionListener, ItemListener
{
    protected InfoSet info;
    protected Project project;

    protected Box bxPages;
    protected ArrayList<PageBox> pages;

    protected JPanel jpEntry;
    protected JComboBox cbCaps;
    protected JComboBox cbLang;
    protected JButton bnNewPage;
    protected JButton bnClose;

    protected PageBox selPb;

    public InfoTool (Project project)
    {
        this(project.getInfoSet(), project);
    }

    public InfoTool (InfoSet info, Project project)
    {
        super(new BorderLayout());
        this.info = info;
        this.project = project;

        jpEntry = new JPanel(new BorderLayout());
        bxPages = Box.createVerticalBox();
        pages = new ArrayList<PageBox>();
        cbCaps = new JComboBox();
        cbLang = new JComboBox();
        bnNewPage = new JButton(BnacEditor.gs("NEW"));
        bnClose = new JButton(BnacEditor.gs("CLOSE"));
        selPb = null;

        buildUi();

        int i=0;
        Capture[] caps = project.getMuseumEnvironment().captureArray();
        String[] capStrs = new String[caps.length];
        PageBox pb;
        InfoSet.InfoPage ip;

        i=0;
        for (Capture ca: project.getMuseumEnvironment().captureArray()) {
            capStrs[i++] = String.format("%s - %s",
                    ca.getReferencedObject().get("Author"),
                    ca.getReferencedObject().get("Title"));
        }
        cbCaps.setModel(new DefaultComboBoxModel(capStrs));

        cbLang.setModel(new DefaultComboBoxModel(Language.values()));

        if (caps.length > 0) { loadCapture(); }

        cbCaps.addItemListener(this);
        cbLang.addItemListener(this);
        bnNewPage.addActionListener(this);
        bnClose.addActionListener(this);
    }

    protected void buildUi ()
    {
        JPanel jpHead = new JPanel(new GridBagLayout());
        JPanel jpCapPageList = new JPanel(new GridBagLayout());

        JScrollPane spPages = new JScrollPane(bxPages,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(2, 5, 2, 5);
        c.weightx = 1.0;
        c.weighty = 0.0;
        c.ipady = 3;

        jpHead.add(cbCaps, c);

        c.gridx = 1;
        jpHead.add(cbLang, c);

        c = new GridBagConstraints();
        c.gridx = c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(2, 5, 2, 5);
        c.weightx = 1.0;
        c.weighty = 0.0;
        c.ipady = 3;

        jpCapPageList.add(new JLabel("Pages:"), c);

        c.gridy = 1;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        jpCapPageList.add(spPages, c);

        c.gridheight = 1;
        c.gridy = 2;
        c.weighty = 0.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        jpCapPageList.add(bnNewPage, c);

        add(jpHead, BorderLayout.NORTH);
        add(jpCapPageList, BorderLayout.WEST);
        add(bnClose, BorderLayout.SOUTH);
        add(jpEntry, BorderLayout.CENTER);

        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    }

    protected void loadCapture ()
    {
        Capture[] caps = project.getMuseumEnvironment().captureArray();
        Capture c = caps[cbCaps.getSelectedIndex()];
        InfoSet.InfoPage ip;
        PageBox pb;

        bxPages.removeAll();
        pages.clear();

        int i=1;
        BnacEditor.trace("starting at infopage %s", c.getId());
        if (info.hasPage(c.getId())) {
            ip = info.getPage(c.getId());
            pb = new PageBox(ip, i++);
            bxPages.add(pb);
            pages.add(pb);

            if (ip.getNext() != null) {
                BnacEditor.trace("%s -> %s", ip.getId(), ip.getNext());
            } else {
                BnacEditor.trace("%s -> null", ip.getId());
            }

            while (ip.getNext() != null) {
                ip = info.getPage(ip.getNext());
                pb = new PageBox(ip, i++);
                bxPages.add(pb);
                pages.add(pb);

                if (ip.getNext() != null) {
                    BnacEditor.trace("%s -> %s", ip.getId(), ip.getNext());
                } else {
                    BnacEditor.trace("%s -> null", ip.getId());
                }
            }
        } else {
            BnacEditor.trace("%s does not exist", c.getId());
        }

        jpEntry.removeAll();

        if (selPb != null) {
            selPb.setSelected(false);
            selPb = null;
        }

        if (SwingUtilities.getWindowAncestor(this) != null) {
            SwingUtilities.getWindowAncestor(this).pack();
        }
        setVisible(false);
        setVisible(true);
    }

    protected void loadEntry (PageBox pb)
    {
        if (selPb != null) { selPb.setSelected(false); }

        selPb = pb;
        selPb.setSelected(true);

        InfoSet.InfoPage ip = pb.getPage();
        BnacEditor.trace("loading entry %s:%s", ip.getId(),
                ((Language) cbLang.getSelectedItem()).getCode());
        InfoSet.InfoEntry e = ip.getEntry(
                ((Language) cbLang.getSelectedItem()).getCode());

        jpEntry.removeAll();

        if (e != null) {
            jpEntry.add(new JScrollPane(new EntryPanel(ip, e),
                        JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                    BorderLayout.CENTER);
        } else {
            JPanel jp = new JPanel(new GridBagLayout());
            jp.setBorder(BorderFactory.createEtchedBorder());
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.NONE;
            JButton cr = new JButton(BnacEditor.gs("CREATE_ENTRY"));
            jp.add(cr, c);
            jpEntry.add(jp, BorderLayout.CENTER);

            cr.addActionListener(new ActionListener () {
                public void actionPerformed (ActionEvent e) {
                    EntryPanel ep = new EntryPanel(selPb.getPage(),
                        selPb.getPage().makeEntry(
                            ((Language) cbLang.getSelectedItem()).
                            getCode()));
                    jpEntry.removeAll();
                    jpEntry.add(ep, BorderLayout.CENTER);
                    SwingUtilities.getWindowAncestor(cbLang).pack();
                    jpEntry.setVisible(false);
                    jpEntry.setVisible(true);
                }
            });
        }

        if (SwingUtilities.getWindowAncestor(this) != null) {
            SwingUtilities.getWindowAncestor(this).pack();
        }
        setVisible(false);
        setVisible(true);
    }

    public void createPage (String id)
    {
        InfoSet.InfoPage ip;
        if (info.getPage(id) != null) {
            ip = info.makePage(String.format(
                        "%s_%d", id, System.currentTimeMillis()));
        } else {
            ip = info.makePage(id);
        }

        PageBox pb = new PageBox(ip, pages.size()+1);

        if (pages.size() > 0) {
            pages.get(pages.size()-1).getPage().setNext(ip.getId());
        }

        pages.add(pb);
        bxPages.add(pb);

        linkAndId();
    }

    public InfoSet.InfoPage removePage (String id)
    {
        InfoSet.InfoPage ip = null;

        for (int i=0; i<pages.size(); ++i) {
            if (pages.get(i).getPageId().equals(id)) {
                info.removePage(id);
                bxPages.remove(pages.get(i));
                ip = pages.remove(i).getPage();
            }
        }

        linkAndId();

        if (selPb != null && selPb.getPageId().equals(ip.getId())) {
            jpEntry.removeAll();
        }

        return ip;
    }

    public void movePage (String id, int idx)
    {
        for (int i=0; i<pages.size(); ++i) {
            if (pages.get(i).getPageId().equals(id)) {
                PageBox cb = pages.remove(i);
                bxPages.remove(i);
                if (idx < pages.size()) {
                    pages.add(idx, cb);
                    bxPages.add(cb, idx);
                } else {
                    pages.add(cb);
                    bxPages.add(cb);
                }
                break;
            }
        }

        linkAndId();
    }

    protected void linkAndId ()
    {
        if (pages.size() == 0) {
            SwingUtilities.getWindowAncestor(this).pack();
            setVisible(false);
            setVisible(true);
            return;
        }

        String capId = project.getMuseumEnvironment().captureArray()[
            cbCaps.getSelectedIndex()].getId();

        String id;
        pages.get(0).getPage().setId(capId);
        for (int i=0; i<pages.size(); ++i) {
            if (i > 0) {
                id = String.format("%s_%d", capId,
                        System.currentTimeMillis());
                while (info.hasPage(id)) {
                    id = String.format("%s_%d", capId,
                            System.currentTimeMillis());
                }
                pages.get(i).getPage().setId(id);

                pages.get(i-1).getPage().setNext(pages.get(i).getPageId());
                BnacEditor.trace("%s -> %s",
                        pages.get(i-1).getPageId(),
                        pages.get(i).getPageId());
            }

            if (i == pages.size()-1) {
                pages.get(i).getPage().setNext(null);
                BnacEditor.trace("%s -> null",
                        pages.get(i).getPage().getId());
            }

            pages.get(i).setIndex(i+1);
        }

        SwingUtilities.getWindowAncestor(this).pack();
        setVisible(false);
        setVisible(true);
    }

    public void moveBackPage (String id)
    {
        for (int i=0; i<pages.size(); ++i) {
            if (pages.get(i).getPageId().equals(id)) {
                if (i > 0) { movePage(id, i-1); }
                return;
            }
        }
    }

    public void moveForwardPage (String id)
    {
        for (int i=0; i<pages.size(); ++i) {
            if (pages.get(i).getPageId().equals(id)) {
                if (i < pages.size()) { movePage(id, i+1); }
                return;
            }
        }
    }

    public void actionPerformed (ActionEvent e)
    {
        if (e.getSource() == bnNewPage) {
            Capture c = project.getMuseumEnvironment().captureArray()[
                cbCaps.getSelectedIndex()];
            createPage(c.getId());
        } else if (e.getSource() == bnClose) {
            SwingUtilities.getWindowAncestor(this).setVisible(false);
        }
    }

    public void itemStateChanged (ItemEvent e)
    {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (e.getSource() == cbCaps) {
                BnacEditor.debug("selected %s", (String) e.getItem());
                loadCapture();
            } else if (e.getSource() == cbLang) {
                if (selPb != null) { loadEntry(selPb); }
                for (PageBox pb: pages) { pb.updateLabel(); }

                SwingUtilities.getWindowAncestor(this).pack();
                setVisible(false);
                setVisible(true);
            }
        }
    }

    class PageBox extends JPanel
        implements ActionListener
    {
        protected InfoSet.InfoPage page;
        protected int idx;

        protected JLabel lblTtl;
        protected JButton bnUp;
        protected JButton bnDown;
        protected JButton bnView;
        protected JButton bnDel;

        public PageBox (InfoSet.InfoPage page, int idx)
        {
            super(new GridBagLayout());
            this.page = page;
            this.idx = idx;

            lblTtl = new JLabel();
            bnUp = new JButton(BnacEditor.gs("UP"));
            bnDown = new JButton(BnacEditor.gs("DOWN"));
            bnView = new JButton(BnacEditor.gs("VIEW"));
            bnDel = new JButton(BnacEditor.gs("DELETE"));

            buildUi();
            updateLabel();

            bnUp.addActionListener(this);
            bnDown.addActionListener(this);
            bnView.addActionListener(this);
            bnDel.addActionListener(this);
        }

        public void buildUi ()
        {
            GridBagConstraints c = new GridBagConstraints();

            c.anchor = GridBagConstraints.LINE_START;
            c.gridx = c.gridy = 0;
            c.gridwidth = 3;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(2, 2, 2, 2);
            c.weightx = 1.0;
            c.weighty = 1.0;
            c.ipady = 3;
            c.ipadx = 3;
            add(lblTtl, c);

            c.weightx = 0.0;
            c.ipady = c.ipadx = 0;
            c.gridwidth = 1;
            c.gridx = GridBagConstraints.RELATIVE;
            add(bnUp, c);

            add(bnDown, c);

            add(bnView, c);

            add(bnDel, c);

            setBorder(BorderFactory.createEtchedBorder());

            setMaximumSize(getPreferredSize());

            java.awt.Dimension d = getPreferredSize();
            d.width = Integer.MAX_VALUE;
            setMaximumSize(d);
        }

        public void setIndex (int idx)
        {
            this.idx = idx;
            updateLabel();
        }

        public void updateLabel ()
        {
            String lang =
                Language.values()[cbLang.getSelectedIndex()].getCode();
            if (page.getEntry(lang) != null &&
                    page.getEntry(lang).title != null) {
                lblTtl.setText(String.format("%d.- %s", idx,
                            page.getEntry(lang).title));
            } else {
                lblTtl.setText(String.format("%d.-", idx));
            }
        }

        public InfoSet.InfoPage getPage () { return page; }

        public String getPageId () { return page.getId(); }

        public void setSelected (boolean sel)
        {
            if (sel) {
                setBorder(BorderFactory.createLineBorder(
                            java.awt.Color.BLACK, 3));
            } else {
                setBorder(BorderFactory.createEtchedBorder());
            }
        }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bnUp) {
                moveBackPage(page.getId());
            } else if (e.getSource() == bnDown) {
                moveForwardPage(page.getId());
            } else if (e.getSource() == bnView) {
                loadEntry(this);
                setSelected(true);
            } else if (e.getSource() == bnDel) {
                removePage(page.getId());
            }
        }
    }

    class EntryPanel extends JPanel
        implements ActionListener
    {
        protected InfoSet.InfoPage page;
        protected InfoSet.InfoEntry entry;

        protected JTextField tfTitle;
        protected JCheckBox cxUseImg;
        protected JTextField tfImage;
        protected JButton bnBrowse;
        protected JLabel lbImage;
        protected JTextArea taDescr;
        protected JButton bnSave;

        public EntryPanel (InfoSet.InfoPage page, InfoSet.InfoEntry entry)
        {
            super(new GridBagLayout());
            this.page = page;
            this.entry = entry;

            tfTitle = new JTextField();
            cxUseImg = new JCheckBox(BnacEditor.gs("USE_IMAGE"));
            tfImage = new JTextField(25);
            lbImage = new JLabel();
            bnBrowse = new JButton(BnacEditor.gs("BROWSE"));
            taDescr = new JTextArea(10, 4);
            taDescr.setBorder(BorderFactory.createLoweredBevelBorder());
            bnSave = new JButton(BnacEditor.gs("SAVE"));
            lbImage.setHorizontalAlignment(JLabel.CENTER);

            buildUi();

            tfTitle.setText(entry.title);
            if (page.getImage() != null) {
                cxUseImg.setSelected(true);
                tfImage.setText(page.getImage());
                lbImage.setText(null);
                lbImage.setIcon(null);
                try {
                    lbImage.setIcon(new ImageIcon(
                                Util.loadImage(
                                    new File(project.getRootPath(),
                                        page.getImage()), 256)));
                } catch (IOException ioe) {
                    lbImage.setText(BnacEditor.gs("COULD_NOT_LOAD_IMAGE"));
                }
            } else {
                cxUseImg.setSelected(false);
                tfImage.setEditable(false);
                bnBrowse.setEnabled(false);
            }
            taDescr.setText(entry.descr);

            bnSave.addActionListener(this);
            bnBrowse.addActionListener(this);
            cxUseImg.addActionListener(this);
        }

        public void buildUi ()
        {
            GridBagConstraints c = new GridBagConstraints();

            c.anchor = GridBagConstraints.LINE_START;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(2, 2, 2, 2);
            c.gridx = c.gridy = 0;
            c.weightx = c.weighty = 0.0;
            c.ipadx = c.ipady = 3;

            add(new JLabel(BnacEditor.gs("TITLE")), c);

            c.gridx = 1;
            c.gridwidth = 4;
            add(tfTitle, c);

            c.gridx = 1;
            c.gridy = 1;
            c.gridwidth = 4;
            add(cxUseImg, c);

            c.gridx = 0;
            c.gridy = 2;
            c.gridwidth = 1;
            c.weightx = c.weighty = 1.0;
            add(new JLabel(BnacEditor.gs("IMAGE")), c);

            c.gridx = 1;
            c.gridwidth = 3;
            c.weightx = c.weighty = 0.0;
            add(tfImage, c);

            c.gridx = 4;
            c.gridwidth = 1;
            add(bnBrowse, c);

            c.gridx = 0;
            c.gridy = 3;
            c.gridwidth = c.gridheight = 5;
            c.fill = GridBagConstraints.BOTH;
            add(lbImage, c);

            c.gridwidth = c.gridheight = 1;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 0;
            c.gridy = 8;
            c.weightx = c.weighty = 1.0;
            add(new JLabel(BnacEditor.gs("DESCRIPTION")), c);

            c.gridwidth = 5;
            c.gridheight = 5;
            c.gridx = 0;
            c.gridy = 9;
            c.fill = GridBagConstraints.BOTH;
            add(new JScrollPane(taDescr,
                        JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                    c);

            c.gridx = 4;
            c.gridy = 14;
            c.gridwidth = 1;
            c.fill = GridBagConstraints.NONE;
            c.anchor = GridBagConstraints.EAST;
            add(bnSave, c);
        }

        public void actionPerformed (ActionEvent e)
        {
            if (e.getSource() == bnSave) {
                entry.title = tfTitle.getText();
                entry.descr = taDescr.getText();
                selPb.updateLabel();
                SwingUtilities.getWindowAncestor(this).pack();
                setVisible(false);
                setVisible(true);
                if (cxUseImg.isSelected() && tfImage.getText().length() > 0) {
                    page.setImage(tfImage.getText());
                } else if (page.getImage() != null &&
                        page.getImage().length() > 0) {
                    File img =
                        new File(project.getRootPath(), page.getImage());
                    if (!img.delete()) {
                        BnacEditor.error("could not delete image %s",
                                page.getImage());
                    }
                    page.setImage(null);
                }
            } else if (e.getSource() == bnBrowse) {
                JFileChooser jfc = new JFileChooser(
                        BnacEditor.config.getProperty("last.dir"));
                jfc.setFileFilter(new FileNameExtensionFilter(
                            Util.gs("IMAGE_FILES"), "jpg", "jpeg", "png"));
                ImagePreview ip = new ImagePreview();
                jfc.addPropertyChangeListener(ip);
                jfc.setAccessory(ip);

                if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    BnacEditor.config.setProperty("last.dir",
                        jfc.getSelectedFile().getParent());
                    selectImage(jfc.getSelectedFile());

                    tfImage.setText(String.format("extra/%s",
                                jfc.getSelectedFile().getName()));

                    SwingUtilities.getWindowAncestor(this).pack();
                    SwingUtilities.getWindowAncestor(this).setVisible(false);
                    SwingUtilities.getWindowAncestor(this).setVisible(true);
                }
            } else if (e.getSource() == cxUseImg) {
                bnBrowse.setEnabled(cxUseImg.isSelected());
                if (cxUseImg.isSelected()) {
                    if (page.getImage() != null) {
                        tfImage.setText(page.getImage());
                        try {
                            lbImage.setIcon(new ImageIcon(
                                        Util.loadImage(
                                            new File(project.getRootPath(),
                                                page.getImage()), 256)));
                        } catch (IOException ioe) {
                            lbImage.setText(
                                    BnacEditor.gs("COULD_NOT_LOAD_IMAGE"));
                        }
                    }
                } else {
                    lbImage.setIcon(null);
                    tfImage.setText("");
                }
            }
        }

        public void selectImage (File imgFile)
        {
            try {
                File imgDest;
                if (page.getImage() != null && page.getImage().length() > 0) {
                    imgDest =
                        new File(project.getRootPath(), page.getImage());
                } else {
                    imgDest = new File(
                            new File(project.getRootPath(), "extra"),
                            imgFile.getName());
                    page.setImage(String.format("extra/%s",
                                imgFile.getName()));
                }
                FileInputStream fin = new FileInputStream(imgFile);
                FileOutputStream fout = new FileOutputStream(imgDest);

                int b;
                while ((b = fin.read()) > -1) { fout.write(b); }
                fout.close();

                lbImage.setIcon(new ImageIcon(
                            Util.loadImage(imgDest, 256)));
            } catch (IOException ioe) {
                BnacEditor.error("could not copy image: %s",
                        ioe.getMessage());
            }
        }
    }

    class ImagePreview extends JComponent
            implements PropertyChangeListener
    {
        Image img;

        public ImagePreview ()
        {
            setPreferredSize(new Dimension(150, 150));
        }

        public void paintComponent (Graphics g)
        {
            if (img != null) { g.drawImage(img, 10, 10, 130, 130, this); }
        }

        public void propertyChange (PropertyChangeEvent e)
        {
            if (e.getPropertyName().equals(
                        JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)) {
                try {
                    img = ImageIO.read((File) e.getNewValue()).
                        getScaledInstance(130, -1, BufferedImage.SCALE_FAST);
                    repaint();
                } catch (Exception ex) {
                    BnacEditor.error("can't load %s: %s",
                            ((File) e.getNewValue()).getAbsolutePath(),
                            ex.getMessage());
                }
            } else if (e.getPropertyName().equals(
                        JFileChooser.DIRECTORY_CHANGED_PROPERTY)) {
                img = null;
                repaint();
            }
        }
    }
}

