/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.editor;

import cl.uchile.dcc.bnac.Door;
import cl.uchile.dcc.bnac.Hall;
import cl.uchile.dcc.bnac.Link;
import cl.uchile.dcc.bnac.editor.event.WallEvent;
import cl.uchile.dcc.bnac.editor.event.WallListener;

import java.util.ArrayList;
import java.util.Hashtable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Stroke;
import java.awt.Cursor;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.vecmath.Point2i;
import javax.vecmath.Point2f;
import javax.vecmath.Vector2f;

public class CreateLinkDialogue extends ProjectDialogue
    implements ActionListener
{
    protected HallGraph hg1;
    protected HallGraph hg2;

    protected JTextField tf_wid;
    protected JTextField tf_hei;

    protected JButton bn_hg1;
    protected JButton bn_hg2;

    protected DoorPlacerPlot dp_hg1;
    protected DoorPlacerPlot dp_hg2;

    protected JButton bn_ok;
    protected JButton bn_ccl;

    public CreateLinkDialogue (Project project)
    {
        super(project, BnacEditor.gs("CREATE_LINK"));
        hg1 = project.getCurrentHallGraph();
        for (Hall  h: project.getMuseumEnvironment().hallArray()) {
            if (!hg1.getId().equals(h.getId())) {
                hg2 = project.getHallView().getHallGraph(h.getId());
                break;
            }
        }

        setLayout(new GridBagLayout());

        tf_wid = new JTextField("1.50");
        tf_hei = new JTextField("2.00");

        bn_hg1 = new JButton(hg1.getHall().getName());
        bn_hg2 = new JButton(hg2.getHall().getName());

        dp_hg1 = new DoorPlacerPlot(hg1.getHall(), 150, 150, 5, 5);
        dp_hg2 = new DoorPlacerPlot(hg2.getHall(), 150, 150, 5, 5);

        bn_ok = new JButton(BnacEditor.gs("OK"));
        bn_ccl = new JButton(BnacEditor.gs("CANCEL"));

        bn_hg1.addActionListener(this);
        bn_hg2.addActionListener(this);
        bn_ok.addActionListener(this);
        bn_ccl.addActionListener(this);

        buildForm();
    }

    protected void buildForm ()
    {
        Box b;

        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(2, 5, 2, 5);
        c.gridx = c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        c.gridwidth = 6;

        b = new Box(BoxLayout.LINE_AXIS);
        b.add(Box.createHorizontalGlue());
        b.add(new JLabel(String.format("%s:",
                    BnacEditor.gs("DOOR_DIMENSIONS")), JLabel.TRAILING));
        b.add(Box.createHorizontalStrut(5));
        b.add(tf_wid);
        b.add(Box.createHorizontalStrut(5));
        b.add(new JLabel("x", JLabel.CENTER));
        b.add(Box.createHorizontalStrut(5));
        b.add(tf_hei);
        b.add(Box.createHorizontalGlue());
        add(b, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        add(new JLabel(String.format("%s:", BnacEditor.gs("HALL")),
                    JLabel.TRAILING), c);

        c.gridx = GridBagConstraints.RELATIVE;
        c.gridwidth = 2;
        c.weightx = 1.0;
        add(bn_hg1, c);

        c.gridwidth = 1;
        c.weightx = 0.0;
        add(new JLabel(String.format("%s:", BnacEditor.gs("HALL")),
                    JLabel.TRAILING), c);

        c.gridx = GridBagConstraints.RELATIVE;
        c.gridwidth = 2;
        c.weightx = 1.0;
        add(bn_hg2, c);

        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = c.gridheight = 3;
        c.weightx = c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        add(dp_hg1, c);
        c.gridx = 3;
        add(dp_hg2, c);

        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 6;
        b = new Box(BoxLayout.LINE_AXIS);
        b.add(Box.createHorizontalGlue());
        b.add(bn_ok);
        b.add(bn_ccl);
        add(b, c);
    }

    public void changeHallGraph (String id, HallGraph hg)
    {
        if (hg1.getId().equals(id)) {
            hg1 = hg;
            dp_hg1.setHall(hg1.getHall());
        } else if (hg2.getId().equals(id)) {
            hg2 = hg;
            dp_hg2.setHall(hg2.getHall());
        }
    }

    public void actionPerformed (ActionEvent e)
    {
        if (e.getSource() == bn_hg1) {
            ProjectDialogue.fireDialogue(new ChooseHallDialogue(project,
                        hg1.getId(), hg2.getId(), this));
        } else if (e.getSource() == bn_hg2) {
            ProjectDialogue.fireDialogue(new ChooseHallDialogue(project,
                        hg2.getId(), hg1.getId(), this));
        } else if (e.getSource() == bn_ok) {
            String time = String.format("%09d", System.currentTimeMillis());
            Point2f dims = new Point2f(
                    Float.parseFloat(tf_wid.getText()),
                    Float.parseFloat(tf_hei.getText()));
            Point2f pos1 = new Point2f(dp_hg1.offset(), dims.y/2.0f);
            Point2f pos2 = new Point2f(dp_hg2.offset(), dims.y/2.0f);
            Door d1 = new Door(String.format("door_%s_1", time), dims, pos1,
                    0.0f, 1.0f);
            Door d2 = new Door(String.format("door_%s_2", time), dims, pos2,
                    0.0f, 1.0f);
            DoorGraph dg1 = new DoorGraph(project, d1);
            DoorGraph dg2 = new DoorGraph(project, d2);
            Link l = new Link(String.format("l_%s", time),
                    d1.getId(), d2.getId());

            project.pushCommand(new CreateLinkCommand(project, l,
                        hg1, dp_hg1.wallIndex(), dg1,
                        hg2, dp_hg2.wallIndex(), dg2));

            jf.setVisible(false);
        } else if (e.getSource() == bn_ccl) {
            jf.setVisible(false);
        }
    }

    public class DoorPlacerPlot extends PlotCanvas
            implements WallListener, MouseMotionListener
    {
        protected float width;
        protected int selW;
        protected float offset;
        protected float of2;

        protected Color hlColor;

        public DoorPlacerPlot (Hall hall, int mx, int my, int bx, int by)
        {
            super (hall, mx, my, bx, by);

            width = 1.5f;
            offset = 0.0f;
            selW = 0;

            hlColor = Color.RED;

            addWallListener(this);
            addMouseMotionListener(this);
        }

        public int wallIndex () { return selW; }

        public float offset () { return offset; }

        public void setHall (Hall hall)
        {
            this.hall = hall;
            updateValues();
            selW = 0;
            width = 1.5f;
            offset = 0.0f;
            repaint();
        }

        public void paint (Graphics g)
        {
            super.paint(g);

            paintDoor(g, selW, offset);

            if (ovrW > -1) { paintDoor(g, ovrW, of2); }
        }

        protected void paintDoor (Graphics g, int widx, float offset)
        {
            Vector2f v = new Vector2f();
            Point2f tmp = new Point2f();
            v.sub(hall.getWall(widx).end2f(), hall.getWall(widx).origin2f());
            float d = v.length();

            tmp.scaleAdd(offset/d, v, hall.getWall(widx).origin2f());
            Point2i l = coordsToPix(tmp.x, tmp.y);
            tmp.scaleAdd((offset+width)/d, v, hall.getWall(widx).origin2f());
            Point2i r = coordsToPix(tmp.x, tmp.y);

            Graphics2D g2 = (Graphics2D) g;
            Stroke os = g2.getStroke();
            g2.setColor(hlColor);
            g2.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE,
                        BasicStroke.JOIN_BEVEL));
            g2.drawLine(l.x, l.y, r.x, r.y);
            g2.setStroke(os);
            g2.setColor(fgColor);
        }

        public void onWallSelected (WallEvent e)
        {
            selW = e.getIndex();
            offset = hall.getWall(selW).origin2f().distance(e.getCoords());
            repaint();
        }

        public void onWallOver (WallEvent e)
        {
            of2 = hall.getWall(ovrW).origin2f().distance(e.getCoords());
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            repaint();
        }

        public void onWallOut (WallEvent e)
        {
            setCursor(Cursor.getDefaultCursor());
            repaint();
        }

        public void mouseMoved (MouseEvent e)
        {
            super.mouseMoved(e);

            if (ovrW > -1) {
                of2 = hall.getWall(ovrW).origin2f().distance(
                        pixToCoords(e.getX(), e.getY()));
                repaint();
            }
        }

        public void mouseDragged (MouseEvent e) { super.mouseDragged(e); }
    }

    public class ChooseHallDialogue extends ProjectDialogue
            implements ActionListener
    {
        protected Hashtable<JButton, String> ids;
        protected CreateLinkDialogue cld;
        protected String change;
        protected String ignore;

        public ChooseHallDialogue (Project project, String change,
                String ignore, CreateLinkDialogue cld)
        {
            super(project, BnacEditor.gs("CHOOSE_HALL"));
            ids = new Hashtable<JButton, String>();
            this.change = change;
            this.ignore = ignore;
            this.cld = cld;

            setLayout(new BorderLayout());

            Box b = new Box(BoxLayout.PAGE_AXIS);
            JButton bn;
            for (Hall h: project.getMuseumEnvironment().hallArray()) {
                if (!h.getId().equals(ignore) && !h.getId().equals(change)) {
                    bn = new JButton(h.getName(),
                            new ImageIcon(
                                PlotCanvas.makeImage(h, 100, 100, 1, 1)));
                    bn.setVerticalTextPosition(JButton.TOP);
                    bn.setHorizontalTextPosition(JButton.CENTER);
                    bn.addActionListener(this);
                    b.add(bn);
                    ids.put(bn, h.getId());
                }
            }

            add(new JScrollPane(b,
                        JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
                    BorderLayout.CENTER);
        }

        public void actionPerformed (ActionEvent e)
        {
            try {
                cld.changeHallGraph(change,
                        project.getHallView().getHallGraph(
                            ids.get((JButton) e.getSource())));
                BnacEditor.trace("selected %s",
                        ids.get((JButton) e.getSource()));
                jf.setVisible(false);
            } catch (ClassCastException cce) {
                return;
            } catch (Exception ex) {
                BnacEditor.warn("%s", ex.getMessage());
                jf.setVisible(false);
            }
        }
    }

    abstract public class LinkCommand extends ProjectCommand
    {
        protected Link link;

        protected HallGraph hg1;
        protected int widx1;
        protected DoorGraph dg1;

        protected HallGraph hg2;
        protected int widx2;
        protected DoorGraph dg2;

        public LinkCommand (Project project, String name, Link link,
                HallGraph hg1, int widx1, DoorGraph dg1,
                HallGraph hg2, int widx2, DoorGraph dg2)
        {
            super(project, name);
            this.link = link;
            
            this.hg1 = hg1;
            this.widx1 = widx1;
            this.dg1 = dg1;

            this.hg2 = hg2;
            this.widx2 = widx2;
            this.dg2 = dg2;
        }
    }

    public class CreateLinkCommand extends LinkCommand
    {
        public CreateLinkCommand (Project project, Link link,
                HallGraph hg1, int widx1, DoorGraph dg1,
                HallGraph hg2, int widx2, DoorGraph dg2)
        {
            super(project, "CREATE_LINK", link,
                    hg1, widx1, dg1,
                    hg2, widx2, dg2);
        }

        public void execute ()
        {
            hg1.addDoorGraph(widx1, dg1);
            hg2.addDoorGraph(widx2, dg2);
            project.getMuseumEnvironment().addLink(link);
            project.getNavigationView().addLink(link);
        }

        public void undo ()
        {
            project.getMuseumEnvironment().removeLink(link.getId());
            hg1.removeDoorGraph(dg1.getId());
            hg2.removeDoorGraph(dg2.getId());
            project.getNavigationView().removeLink(link.getId());
        }
    }
}

